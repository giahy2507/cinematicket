@extends('backend.master')

@section('title', 'Dashboard')

@section('content')
<div class="content" ng-controller="MovieCtrl">
    <div class="row">
        <div class="panel panel-default" >
            <div class="panel-heading">
            <h2>
                <span>Movies </span>
                <span class="badge" ng-show="movies.length"> <%movies.length%> </span>
                <button ng-click="addNewModal()" class="btn btn-primary pull-right">Add new movie</button>
            </h2>
            </div>

            <div class="panel-body" >
                <table class="table table-condensed table-stripe ddt-responsive">
                    <thead>
                      <tr>
                        <th> # </th>
                        <th> Name </th>
                        <th> Release date </th>
                        <th> Genres </th>
                        <th> </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="movie in movies">
                        <td><%movie.id%></td>
                        <td><%movie.name%></td>
                        <td><%movie.release_date%></td>
                        <td><%movie.genres%></td>
                        <td>
                            <button ng-click="editModal(movie)" class="btn btn-warning pull-right">Edited</button>
                            <button class="btn btn-danger btn-delete pull-right" ng-click="delete(movie.id)"><span>Delete</span></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
    <modal title="Movie" visible="showModal">
        <form class="form-horizontal" role="form">
            <div class="form-group">
                <label class="col-md-4 control-label" for="name">Name</label>
                <div class="col-md-6">
                    <input class="form-control" id="name" placeholder="Enter movie name" ng-model="movie.name"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="release_date">Release date</label>
                <div class="col-md-6">
                    <input class="form-control" id="release_date" placeholder="Release date, format: yyyy-mm-dd, ex. 2016-01-03" ng-model="movie.release_date"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="genres">Genres</label>
                <div class="col-md-6">
                    <input class="form-control" id="genres" placeholder="Genres" ng-model="movie.genres"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button ng-click="add(movie)" class="btn btn-default"><%btn_name%></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </modal>
</div>
@endsection