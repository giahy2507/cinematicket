@extends('backend.master')

@section('title', 'Dashboard')

@section('content')
<div class="content" ng-controller="SeatCtrl">
    <div class="row">
        <div class="panel panel-primary" >
            <div class="panel-heading">
                <span ng-click="showCinemas = ! showCinemas" class="btn">Show Cinemas </span>
            </div>

            <div class="panel-body" ng-show="showCinemas">
                <table class="table table-condensed table-stripe ddt-responsive">
                    <thead>
                        <tr>
                            <th> # </th>
                            <th> Cinema </th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="cinema in cinemas">
                            <td><%cinema.id%></td>
                            <td><%cinema.name%></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="panel panel-default" >
            <div class="panel-heading">
            <h2>
                <span>Seats </span>
                <span class="badge" ng-show="seats.length"> <%seats.length%> </span>
                <button ng-click="addNewModal()" class="btn btn-primary pull-right">Add new seat</button>
            </h2>
            </div>

            <div class="panel-body" >
                <table class="table table-condensed table-stripe ddt-responsive">
                    <thead>
                      <tr>
                        <th> # </th>
                        <th> Number </th>
                        <th> Cinema </th>
                        <th> </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="seat in seats">
                        <td><%seat.id%></td>
                        <td><%seat.seat_number%></td>
                        <td><%seat.cinema_id%></td>
                        <td>
                            <button ng-click="editModal(seat)" class="btn btn-warning pull-right">Edited</button>
                            <button class="btn btn-danger btn-delete pull-right" ng-click="delete(seat.id)"><span>Delete</span></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
    <modal title="Seat" visible="showModal">
        <form class="form-horizontal" role="form">
            <div class="form-group">
                <label class="col-md-4 control-label" for="seat_number">Seat number</label>
                <div class="col-md-6">
                    <input class="form-control" id="seat_number" placeholder="Enter seat seat_number" ng-model="seat.seat_number"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="cinema">Cinema</label>
                <div class="col-md-6">
                    <select ng-model="selectedCinema" ng-options="c.name for c in cinemas" ng-change="changedCinema(selectedCinema)"></select>
                    <!-- <input class="form-control" id="cinema" placeholder="cinema" ng-model="seat.cinema_id"/> -->
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button ng-click="add(seat)" class="btn btn-default"><%btn_name%></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </modal>
</div>
@endsection