@extends('backend.master')

@section('title', 'Dashboard')

@section('content')
<div class="content" ng-controller="UserCtrl">
    <div class="row">
        <div class="panel panel-default" >
            <div class="panel-heading">
            <h2>
                <span>Users </span>
                <span class="badge" ng-show="users.length"> <%users.length%> </span>
                <button ng-click="addNewModal()" class="btn btn-primary pull-right">Add new user</button>
            </h2>
            </div>

            <div class="panel-body" >
                <table class="table table-condensed table-stripe ddt-responsive">
                    <thead>
                      <tr>
                        <th> # </th>
                        <th> User name </th>
                        <th> Name </th>
                        <th> Email </th>
                        <th> Phone number </th>
                        <th> Admin </th>
                        <th> Membership </th>
                        <th> </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="user in users">
                        <td><%user.id%></td>
                        <td><%user.user_name%></td>
                        <td><%user.name%></td>
                        <td><%user.email%></td>
                        <td><%user.phone_number%></td>
                        <td><%user.admin%></td>
                        <td><%user.membership%></td>
                        <td>
                            <button ng-click="editModal(user)" class="btn btn-warning pull-right">Edited</button>
                            <button class="btn btn-danger btn-delete pull-right" ng-click="delete(user.id)"><span>Delete</span></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
    <modal title="User" visible="showModal">
        <form class="form-horizontal" role="form">
            <div class="form-group">
                <label class="col-md-4 control-label" for="user_name">User name</label>
                <div class="col-md-6">
                    <input class="form-control" id="user_name" placeholder="Enter user name" ng-model="user.user_name"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="name">Name</label>
                <div class="col-md-6">
                    <input class="form-control" id="name" placeholder="Enter name" ng-model="user.name"/>
                </div>
            </div>
            <div class="form-group" ng-show="isAdd">
                <label class="col-md-4 control-label" for="email">Email</label>
                <div class="col-md-6">
                    <input type="email" class="form-control" id="email" placeholder="Email" ng-model="user.email"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="password">Password</label>
                <div class="col-md-6">
                    <input type="password" class="form-control" id="password" placeholder="Password" ng-model="user.password"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="phone_number">Phone number</label>
                <div class="col-md-6">
                    <input class="form-control" id="phone_number" placeholder="Phone number" ng-model="user.phone_number"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="membership">Membership</label>
                <div class="col-md-6">
                    <input class="form-control" id="membership" placeholder="Membership" ng-model="user.membership"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button ng-click="add(user)" class="btn btn-default"><%btn_name%></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </modal>
</div>
@endsection