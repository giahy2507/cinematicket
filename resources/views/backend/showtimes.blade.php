@extends('backend.master')

@section('title', 'Dashboard')

@section('content')
<div class="content" ng-controller="ShowtimeCtrl">
    <div class="row">
        <div class="panel panel-primary" >
            <div class="panel-heading">
                <span ng-click="showMovies = ! showMovies" class="btn">Show Movies</span>
                <span ng-click="showCinemas = ! showCinemas" class="btn">Show Cinemas </span>
            </div>

            <div class="panel-body" ng-show="showMovies">
                <table class="table table-condensed table-stripe ddt-responsive" >
                    <thead>
                      <tr>
                        <th> # </th>
                        <th> Movie </th>
                        <th> </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="movie in movies">
                        <td><%movie.id%></td>
                        <td><%movie.name%></td>
                      </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-body" ng-show="showCinemas">
                <table class="table table-condensed table-stripe ddt-responsive">
                    <thead>
                        <tr>
                            <th> # </th>
                            <th> Cinema </th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="cinema in cinemas">
                            <td><%cinema.id%></td>
                            <td><%cinema.name%></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="panel panel-default" >
            <div class="panel-heading">
            <h2>
                <span>Showtimes </span>
                <span class="badge" ng-show="showtimes.length"> <%showtimes.length%> </span>
                <button ng-click="addNewModal()" class="btn btn-primary pull-right">Add new showtime</button>
            </h2>
            </div>

            <div class="panel-body" >
                <table class="table table-condensed table-stripe ddt-responsive">
                    <thead>
                      <tr>
                        <th> # </th>
                        <th> Type </th>
                        <th> Time </th>
                        <th> Movie </th>
                        <th> Cinema </th>
                        <th> </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="showtime in showtimes">
                        <td><%showtime.id%></td>
                        <td><%showtime.type%></td>
                        <td><%showtime.time%></td>
                        <td><%showtime.movie_id%></td>
                        <td><%showtime.cinema_id%></td>
                        <td>
                            <button ng-click="editModal(showtime)" class="btn btn-warning pull-right">Edited</button>
                            <button class="btn btn-danger btn-delete pull-right" ng-click="delete(showtime.id)"><span>Delete</span></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
    <modal title="Showtime" visible="showModal">
        <form class="form-horizontal" role="form">
            <div class="form-group">
            <div class="form-group">
                <label class="col-md-4 control-label" for="type"><b>Type</b></label>
                <div class="col-md-6">
                    <input class="form-control" id="type" placeholder="sub or dub" ng-model="showtime.type"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="time"><b>Time</b></label>
                <div class="col-md-6">
                    <input class="form-control" id="time" placeholder="Time, format: yyyy-mm-dd hh:mm:ss, ex. 2016-01-03 15:18:40" ng-model="showtime.time"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="movie_id"><b>Movie ID</b></label>
                <div class="col-md-6">
                    <select ng-model="selectedMovie" ng-options="m.name for m in movies" ng-change="changedMovie(selectedMovie)"></select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="cinemas_id"><b>Cinema</b></label>
                <div class="col-md-6">
                    <select ng-model="selectedCinema" ng-options="c.name for c in cinemas" ng-change="changedCinema(selectedCinema)"></select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button ng-click="add(showtime)" class="btn btn-default"><%btn_name%></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </modal>
</div>
@endsection