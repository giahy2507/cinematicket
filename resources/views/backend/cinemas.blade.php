@extends('backend.master')

@section('title', 'Dashboard')

@section('content')
<div class="content" ng-controller="CinemaCtrl">
    <div class="row">
        <div class="panel panel-default" >
            <div class="panel-heading">
            <h2>
                <span>Cinemas </span>
                <span class="badge" ng-show="cinemas.length"> <%cinemas.length%> </span>
                <button ng-click="addNewModal()" class="btn btn-primary pull-right">Add new cinema</button>
            </h2>
            </div>

            <div class="panel-body" >
                <table class="table table-condensed table-stripe ddt-responsive">
                    <thead>
                      <tr>
                        <th> # </th>
                        <th> Name </th>
                        <th> Address </th>
                        <th> Phone number</th>
                        <th> </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="cinema in cinemas">
                        <td><%cinema.id%></td>
                        <td><%cinema.name%></td>
                        <td><%cinema.address%></td>
                        <td><%cinema.phone_number%></td>
                        <td>
                            <button ng-click="editModal(cinema)" class="btn btn-warning pull-right">Edited</button>
                            <button class="btn btn-danger btn-delete pull-right" ng-click="delete(cinema.id)"><span>Delete</span></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
    <modal title="Cinema" visible="showModal">
        <form class="form-horizontal" role="form">
            <div class="form-group">
                <label class="col-md-4 control-label" for="name">Name</label>
                <div class="col-md-6">
                    <input class="form-control" id="name" placeholder="Enter cinema name" ng-model="cinema.name"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="address">Address</label>
                <div class="col-md-6">
                    <input class="form-control" id="address" placeholder="address" ng-model="cinema.address"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="phone_number">Phone number</label>
                <div class="col-md-6">
                    <input class="form-control" id="phone_number" placeholder="Phone number" ng-model="cinema.phone_number"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button ng-click="add(cinema)" class="btn btn-default"><%btn_name%></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </modal>
</div>
@endsection