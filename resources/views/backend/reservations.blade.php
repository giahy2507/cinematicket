@extends('backend.master')

@section('title', 'Dashboard')

@section('content')
<div class="content" ng-controller="ReservationCtrl">
    <div class="row">
        <div class="panel panel-primary" >
            <div class="panel-heading">
                <span ng-click="showShowtimes = ! showShowtimes" class="btn">Show Showtimes</span>
                <span ng-click="showUsers = ! showUsers" class="btn">Show Users </span>
                <span ng-click="showSeats = ! showSeats" class="btn">Show Seats </span>
            </div>

            <div class="panel-body" ng-show="showShowtimes">
                <table class="table table-condensed table-stripe ddt-responsive" >
                    <thead>
                      <tr>
                        <th> # </th>
                        <th> Showtime </th>
                        <th> </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="showtime in showtimes">
                        <td><%showtime.id%></td>
                        <td><%showtime.time%></td>
                      </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-body" ng-show="showUsers">
                <table class="table table-condensed table-stripe ddt-responsive">
                    <thead>
                        <tr>
                            <th> # </th>
                            <th> User </th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="user in users">
                            <td><%user.id%></td>
                            <td><%user.name%></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="panel-body" ng-show="showSeats">
                <table class="table table-condensed table-stripe ddt-responsive">
                    <thead>
                        <tr>
                            <th> # </th>
                            <th> Seat number</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="seat in seats">
                            <td><%seat.id%></td>
                            <td><%seat.seat_number%></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="panel panel-default" >
            <div class="panel-heading">
            <h2>
                <span>Reservations </span>
                <span class="badge" ng-show="reservations.length"> <%reservations.length%> </span>
                <button ng-click="addNewModal()" class="btn btn-primary pull-right">Add new reservation</button>
            </h2>
            </div>

            <div class="panel-body" >
                <table class="table table-condensed table-stripe ddt-responsive">
                    <thead>
                      <tr>
                        <th> # </th>
                        <th> Date </th>
                        <th> Showtime </th>
                        <th> User </th>
                        <th> Seat </th>
                        <th> </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="reservation in reservations">
                        <td><%reservation.id%></td>
                        <td><%reservation.date%></td>
                        <td><%reservation.showtime_id%></td>
                        <td><%reservation.user_id%></td>
                        <td><%reservation.seat_id%></td>
                        <td>
                            <button ng-click="editModal(reservation)" class="btn btn-warning pull-right">Edited</button>
                            <button class="btn btn-danger btn-delete pull-right" ng-click="delete(reservation.id)"><span>Delete</span></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
    <modal title="Reservation" visible="showModal">
        <form class="form-horizontal" role="form">
            <div class="form-group">
            <div class="form-group">
                <label class="col-md-4 control-label" for="date">Date</label>
                <div class="col-md-6">
                    <input class="form-control" id="date" placeholder="yyyy-mm-dd, ex. 2016-01-03" ng-model="reservation.date"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="showtime_id">Showtime</label>
                <div class="col-md-6">
                    <select ng-model="selectedShowtime" ng-options="s.time for s in showtimes" ng-change="changedShowtime(selectedShowtime)"></select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="user_id">User</label>
                <div class="col-md-6">
                    <select ng-model="selectedUser" ng-options="u.name for u in users" ng-change="changedUser(selectedUser)"></select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="seat_id">Seat</label>
                <div class="col-md-6">
                    <select ng-model="selectedSeat" ng-options="se.seat_number for se in seats" ng-change="changedSeat(selectedSeat)"></select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button ng-click="add(reservation)" class="btn btn-default"><%btn_name%></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </modal>
</div>
@endsection