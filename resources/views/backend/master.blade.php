<html lang="en" ng-app="adminApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    
    <link href="/css/backend.css" rel="stylesheet" type="text/css" media="all" />
    
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
    

    <!-- Scripts -->
    <script type="text/javascript">

        angular.module('adminApp', ['UserSv', 'ShowtimeSv', 'MovieSv', 'CinemaSv', 'StarSv', 'SeatSv', 'ReservationSv'], function($interpolateProvider) {
            $interpolateProvider.startSymbol('<%');
              $interpolateProvider.endSymbol('%>');
        });
    </script>
    <!-- Angular User Script -->
        <!-- Service -->
    <script src="{{ asset('js/angularjs_backend/UserSv.js') }}"></script>
    <script src="{{ asset('js/angularjs_backend/MovieSv.js') }}"></script>
    <script src="{{ asset('js/angularjs_backend/ShowtimeSv.js') }}"></script>
    <script src="{{ asset('js/angularjs_backend/CinemaSv.js') }}"></script>
    <script src="{{ asset('js/angularjs_backend/SeatSv.js') }}"></script>
    <script src="{{ asset('js/angularjs_backend/ReservationSv.js') }}"></script>
    <script src="{{ asset('js/angularjs_backend/StarSv.js') }}"></script>

        <!-- Ctrl -->
    <script src="{{ asset('js/angularjs_backend/LoginCtrl.js') }}"></script>
    <script src="{{ asset('js/angularjs_backend/MovieCtrl.js') }}"></script>
    <script src="{{ asset('js/angularjs_backend/UserCtrl.js') }}"></script>
    <script src="{{ asset('js/angularjs_backend/HeaderCtrl.js') }}"></script>
    <script src="{{ asset('js/angularjs_backend/ShowtimeCtrl.js') }}"></script>
    <script src="{{ asset('js/angularjs_backend/CinemaCtrl.js') }}"></script>
    <script src="{{ asset('js/angularjs_backend/SeatCtrl.js') }}"></script>
    <script src="{{ asset('js/angularjs_backend/ReservationCtrl.js') }}"></script>
    <script src="{{ asset('js/angularjs_backend/StarCtrl.js') }}"></script>
</head>

<body>
    <nav class="navbar navbar-default" ng-controller="HeaderCtrl">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Admin Dashboard</a>
            </div>

            <div class="collapse navbar-collapse" id="menu">
                <ul class="nav navbar-nav">
                    <li role="presentation" class="dropdown ">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            Dashboard <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li role="presentation" class="">
                                <a href="/admin/users">Users</a>
                            </li>
                            <li role="presentation"  class="">
                                <a href="/admin/movies">Movies</a>
                            </li>
                            <li role="presentation"  class="">
                                <a href="/admin/stars">Stars</a>
                            </li>
                            <li role="presentation"  class="">
                                <a href="/admin/cinemas">Cinemas</a>
                            </li>
                            <li role="presentation"  class="">
                                <a href="/admin/seats">Seats</a>
                            </li>
                            <li role="presentation"  class="">
                                <a href="/admin/showtimes">Showtimes</a>
                            </li>
                            <li role="presentation"  class="">
                                <a href="/admin/reservations">Reservations</a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/admin" ng-hide="admin">Login</a></li>
                    <li role="presentation" class="dropdown" ng-show="admin">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" href="{{ URL::to('/admin/info')}}" style="text-transform: uppercase">
                            <%admin%> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li role="presentation">
                                <a ng-click="logoutRequest()">Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        @yield('content')
    </div>

</body>
</html>