@extends('backend.master')

@section('title', 'Dashboard')

@section('content')
<div class="content" ng-controller="StarCtrl">
    <div class="row">
        <div class="panel panel-default" >
            <div class="panel-heading">
            <h2>
                <span>Stars </span>
                <span class="badge" ng-show="stars.length"> <%stars.length%> </span>
                <button ng-click="addNewModal()" class="btn btn-primary pull-right">Add new star</button>
            </h2>
            </div>

            <div class="panel-body" >
                <table class="table table-condensed table-stripe ddt-responsive">
                    <thead>
                      <tr>
                        <th> # </th>
                        <th> Name </th>
                        <th> Birthday </th>
                        <th> Gender </th>
                        <th> Nationality </th>
                        <th> Deathday </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="star in stars">
                        <td><%star.id%></td>
                        <td><%star.name%></td>
                        <td><%star.birthday%></td>
                        <td><%star.gender%></td>
                        <td><%star.nationality%></td>
                        <td><%star.deathday%></td>
                        <td>
                            <button ng-click="editModal(star)" class="btn btn-warning pull-right">Edited</button>
                            <button class="btn btn-danger btn-delete pull-right" ng-click="delete(star.id)"><span>Delete</span></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
    <modal title="Star" visible="showModal">
        <form class="form-horizontal" role="form">
            <div class="form-group">
                <label class="col-md-4 control-label" for="name">Name</label>
                <div class="col-md-6">
                    <input class="form-control" id="name" placeholder="Enter star name" ng-model="star.name"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="birthday">Birthday</label>
                <div class="col-md-6">
                    <input class="form-control" id="birthday" placeholder="Birthday, format: yyyy-mm-dd, ex. 2016-01-03" ng-model="star.birthday"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="gender">Gender</label>
                <div class="col-md-6">
                    <input class="form-control" id="gender" placeholder="Gender" ng-model="star.gender"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="nationality">Nationality</label>
                <div class="col-md-6">
                    <input class="form-control" id="nationality" placeholder="Nationality" ng-model="star.nationality"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="deathday">Deathday</label>
                <div class="col-md-6">
                    <input class="form-control" id="deathday" placeholder="Deathday, format: yyyy-mm-dd, ex. 2016-01-03" ng-model="star.deathday"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button ng-click="add(star)" class="btn btn-default"><%btn_name%></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </modal>
</div>
@endsection
