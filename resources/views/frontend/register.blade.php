@extends('frontend.master')

@section('title','Register')

@section('header')
    <!-- start top_js_button -->
    <script type="text/javascript" src="js/easing.js"></script>
    <script type="text/javascript" src="js/move-top.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
            });
        });
    </script>
@endsection


@section('content')
    <div class="register_container" ng-controller="UserController">
        <!-- start top_bg -->
        <div class="top_bg">
            <div class="wrap">
                <div class="main_top">
                    <h4 class="style">login or create an account</h4>
                </div>
            </div>
        </div>
        <!-- start main -->
        <div class="main_bg">
            <div class="wrap">
                <div class="main">
                    <div class="login_left">
                        <h3>login customers</h3>
                        <p>if you have any account with us, please log in.</p>
                        <!-- start registration -->
                        <div class="registration">
                            <!-- [if IE]
                                < link rel='stylesheet' type='text/css' href='ie.css'/>
                             [endif] -->

                            <!-- [if lt IE 7]>
                                < link rel='stylesheet' type='text/css' href='ie6.css'/>
                            <! [endif] -->
                            <script>
                                (function() {

                                    // Create input element for testing
                                    var inputs = document.createElement('input');

                                    // Create the supports object
                                    var supports = {};

                                    supports.autofocus   = 'autofocus' in inputs;
                                    supports.required    = 'required' in inputs;
                                    supports.placeholder = 'placeholder' in inputs;

                                    // Fallback for autofocus attribute
                                    if(!supports.autofocus) {

                                    }

                                    // Fallback for required attribute
                                    if(!supports.required) {

                                    }

                                    // Fallback for placeholder attribute
                                    if(!supports.placeholder) {

                                    }

                                    // Change text inside send button on submit
                                    var send = document.getElementById('register-submit');
                                    if(send) {
                                        send.onclick = function () {
                                            this.innerHTML = '...Sending';
                                        }
                                    }

                                })();
                            </script>
                            <div class="registration_left">
                                <a href="{{ URL::to('/auth/facebook')}}"><div class="reg_fb"><img src="images/facebook.png" alt=""><i>sign in using Facebook</i><div class="clear"></div></div></a>
                                <div class="registration_form">
                                    <!-- Form -->
                                    <form id="registration_form" ng-submit="loginRequest()">
                                        <div>
                                            <label>
                                                <input placeholder="Email" type="email" tabindex="" required="" ng-model="user_data.email">
                                            </label>
                                        </div>
                                        <div>
                                            <label>
                                                <input placeholder="Password" type="password" tabindex="" required="" ng-model="user_data.password">
                                            </label>
                                        </div>
                                        <div>
                                            <input type="submit" value="sign in" id="register-submit">
                                        </div>
                                        <div class="forget">
                                            <a href="#">forgot your password</a>
                                        </div>
                                    </form>
                                    <!-- /Form -->
                                </div>
                            </div>
                        </div>
                        <!-- end registration -->
                    </div>
                    <div class="login_left">
                        <h3>register new customers</h3>
                        <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping address, view and track your orders in your accoung and more.</p>
                        <div class="registration_left">
                            <a href="{{ URL::to('/auth/facebook')}}"><div class="reg_fb"><img src="images/facebook.png" alt=""><i>register using Facebook</i><div class="clear"></div></div></a>
                            <div class="registration_form">
                                <!-- Form -->
                                <form id="registration_form" ng-submit="registerRequest()">
                                    <div>
                                        <label>
                                            <input placeholder="First Name" type="text" ng-model="user_data.first_name" tabindex="1" required="" autofocus="">
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input placeholder="Last Name" type="text" ng-model="user_data.last_name" tabindex="2" required="" autofocus="">
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input placeholder="Email Address" type="email" ng-model="user_data.email" tabindex="3" required="">
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input placeholder="Phone Number" type="tel" ng-model="user_data.phone_number" tabindex="4" required="">
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input placeholder="Password" type="password" ng-model="user_data.password" tabindex="5" required="">
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input placeholder="Retype Password" type="password" tabindex="6" required="">
                                        </label>
                                    </div>
                                    <div>
                                        <input type="submit" value="create an account" id="regisx`ter-submit">
                                    </div>
                                    <div class="sky_form">
                                        <label class="checkbox"><input type="checkbox" name="checkbox"><i>I agree to <a class="terms" href="#"> terms of service</a> </i></label>
                                    </div>
                                </form>
                                <!-- /Form -->
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
@endsection