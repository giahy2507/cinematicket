@extends('frontend.master')

@section('title','Cinema')

@section('header')
    <script src="{{ asset('js/angularjs/CinemaController.js') }}" ></script>
@endsection

@section('content')
    <div id="cinema_container" ng-controller="CinemaController">
        <div class="main_bg">
            <div class="wrap">
                <div class="main">
                    <div class="single">
                        <!-- start span1_of_1 -->
                        <div class="left_content" style="width: 100%">
                            <!-- start span1_of_1 -->
                            <div class="span1_of_1_des" style="width:60%">
                                <div class="desc1" ng-repeat="cinema in cinemas">
                                    <h3><%cinema.name%></h3>
                                    <div class="blogsidebar span_2_of_blog" style="width: 100%; margin-bottom: 20px;">
                                        <ul class="blog-list">
                                            <li>Address<br><a href="#"><%cinema.address%></a></li>
                                            <li>Phone number<br><a style="text-transform: capitalize" href="#"><%cinema.phone_number%></a>
                                        </ul>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>

                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
@endsection