@extends('frontend.master')

@section('title', 'User Information')

@section('header')

    <script type="text/javascript">
        @if($access_token)
            localStorage.setItem('access_token','<?php echo $access_token; ?>');
//            window.alert(localStorage.access_token)
        @endif
    </script>
    <script src="{{ asset('js/angularjs/ReservationController.js') }}" ></script>
@endsection

@section('content')
    <div id="register_container" class="register_container" ng-controller = "UserController" >
        <!-- start top_bg -->
        <div class="top_bg">
            <div class="wrap">
                <div class="main_top">
                    <h4 class="style">User Information</h4>
                </div>
            </div>
        </div>
        <!-- start main -->
        <div class="main_bg">
            <div class="wrap">
                <div class="main">
                    <div class="login_center">
                        <h3>Hello Hy Nguyen</h3>
                        <p>You able to move through the checkout process faster, store multiple shipping address, view and track your orders in your accoung and more.</p>
                        <div class="registration_left">
                            <div class="registration_form">
                                <!-- Form -->
                                <form id="registration_form" ng-submit="updateRequest()">
                                    <div>
                                        <label>
                                            <input placeholder="First Name" type="text" ng-model="user_data.name" tabindex="1" required="" autofocus="">
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input placeholder="Email Address" type="email" ng-model="user_data.email" tabindex="3" disabled required="">
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input placeholder="Phone Number" type="tel" ng-model="user_data.phone_number" tabindex="4" required="">
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input placeholder="Membership" disabled type="text" style="text-transform: uppercase;" ng-model="user_data.membership" tabindex="5" required="">
                                        </label>
                                    </div>
                                    <div>
                                        <input type="submit" value="edit" id="update-submit">
                                        <a class="terms" href="/changepassword"> Change Password</a>
                                    </div>
                                </form>
                                <script>
                                    //get User data
                                    angular.element(document.getElementById('register_container')).scope().getuserRequest("1");
                                </script>
                                <!-- /Form -->
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
    <div id="register_container_2" class="register_container" ng-controller = "ReservationController" >
        <!-- start top_bg -->
        <div class="top_bg">
            <div class="wrap">
                <div class="main_top">
                    <h4 class="style"> Reservation </h4>
                </div>
            </div>
        </div>
        <div class="main_bg">
            <div class="wrap">
                <div class="main">
                    <div class="single">
                        <!-- start span1_of_1 -->
                        <div class="left_content" style="width: 100%">
                            <!-- start span1_of_1 -->
                            <div class="span1_of_1_des" style="width:60%">
                                <div class="desc1" ng-repeat="reservation in reservations">
                                    <h3><%reservation.movie_name%></h3>
                                    <div class="blogsidebar span_2_of_blog" style="width: 100%; margin-bottom: 20px;">
                                        <ul class="blog-list">
                                            <li>Theater<br><a href="#"><%reservation.cinema_name%></a></li>
                                            <li>Seat Number<br><a href="#"><%reservation.seat_number%></a></li>
                                            <li>Show time<br><a href="#"><%reservation.showtime%></a></li>
                                        </ul>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>

                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>

    </div>
@endsection
