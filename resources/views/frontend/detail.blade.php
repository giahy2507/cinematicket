@extends('frontend.master')

@section('title', 'Ticket Detail')


@section('header')
    <script src="{{ asset('js/angularjs/MovieController.js') }}" ></script>
@endsection


@section('content')
        <div id="detail_container" class="detail_container" ng-controller = "MovieController">
            <script>
                @if($movie_id)
                    angular.element(document.getElementById('detail_container')).scope().get_movie_by_id('<?php echo $movie_id; ?>');
                    angular.element(document.getElementById('detail_container')).scope().get_cinemas_showtime_accord_movie_id_request('<?php echo $movie_id; ?>');
                @endif

            </script>
            <!-- start top_bg -->
            <div class="top_bg">
                <div class="wrap">
                    <div class="main_top">
                        <h2 class="style">Movie Detail</h2>
                    </div>
                </div>
            </div>

            <!-- start main -->
            <div class="main_bg">
                <div class="wrap">
                    <div class="main">
                        <!-- start content -->
                        <div class="single">
                            <!-- start span1_of_1 -->
                            <div class="left_content" style="width: 100%">
                                <div class="span1_of_1" style="width: 35%">
                                    <img src="<%base_url + movie_detail.poster_url%>" alt="<%movie_detail.name%>" title="<%movie_detail.name%>" />
                                </div>
                                <!-- start span1_of_1 -->
                                <div class="span1_of_1_des" style="width:60%">
                                    <div class="desc1">
                                        <h3><%movie_detail.name%></h3>
                                        <div class="blogsidebar span_2_of_blog" style="width: 100%; margin-bottom: 20px;">
                                            <ul class="blog-list">
                                                <li>Release Date<br><a href="#"><%movie_detail.release_date%></a></li>
                                                <li>Genres<br><a style="text-transform: capitalize" href="#"><%movie_detail.genres%></a>
                                                <li>Actors<br>
                                                    <a style="text-transform: capitalize" href="#" ng-repeat="star in movie_detail.stars"> <%star.name%>&nbsp; &nbsp; </a>
                                                </li>
                                                <li>Running Time<br><a href="#">120 minutes</a></li>
                                            </ul>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="available" style="margin-bottom: 20px">
                                            <a href="#booking_area" class="hybtn">Buy Now</a>
                                        </div>
                                        <div class="share-desc">
                                            <div class="share">
                                                <h4>Share Product :</h4>
                                                <ul class="share_nav">
                                                    <li><a href="#"><img src="{{ asset('images/facebook.png') }}" title="facebook"></a></li>
                                                    <li><a href="#"><img src="{{ asset('images/twitter.png') }}" title="Twiiter"></a></li>
                                                    <li><a href="#"><img src="{{ asset('images/rss.png') }}" title="Rss"></a></li>
                                                    <li><a href="#"><img src="{{ asset('images/gpluse.png') }}" title="Google+"></a></li>
                                                </ul>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>
                    <!-- end content -->
                </div>
            </div>
            <!-- start main -->
            <div class="main_bg">
                <div class="wrap">
                    <div class="main">
                        <div class="login_left" >
                            <div ng-repeat="cinema_showtime in cinemas_showtime_accord_movie_id" class="top_main">
                                <a href="<%base_url+'cinema/'+cinema_showtime.id%>" style="border: none; background: none ; float: left">
                                    <h3 style="color: black; ">
                                        <%cinema_showtime.name%>
                                    </h3>
                                </a>
                                <div class="clear"></div>
                                <ul class="blog-list" >
                                    <li ng-repeat="showtime in cinema_showtime.showtimes">
                                        <a href="#" style="float: left; border: 0px; background: none" > <%showtime.time%> &nbsp; &nbsp; </a></li>
                                </ul>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="login_left" id="booking_area">
                            <div class="top_main" >
                                <h3 style="color: black; ">Chose showing time</h3>
                                <div class="clear"></div>
                                <select name="repeatSelect" ng-change="showtime_change()" id="repeatSelectshowtime" ng-model="booking.showtime_id" style="font-family: 'Maven Pro', sans-serif; font-size: 1em; margin: 2%">
                                    <option ng-repeat="showtime in showtimes" value="<%showtime.id%>"> <%showtime.time%> </option>
                                </select>
                            </div>
                            <h3 style="color: black; ">Price: 55 VND </h3>

                            <div class="top_main">
                                <h3 style="color: black; ">Seating Map</h3>
                                <img src="{{ asset('images/seatingmap.png') }}" style="width: 100%">
                                <h3 style="color: black; ">Chose seat</h3>
                                <div class="clear"></div>
                                <select name="repeatSelect" ng-change="seat_change()" id="repeatSelectseat" ng-model="booking.seat_id" style="font-family: 'Maven Pro', sans-serif; font-size: 1em; margin: 2%">
                                    <option ng-repeat="available_seat in available_seats" value="<%available_seat.id%>"> <%available_seat.seat_number%> </option>
                                </select>
                            </div>
                            <div style="margin-top: 50px; text-align: center">
                                <a class="hybtn" style="margin-top: 5%" ng-click="book_ticket_requets()">Book Ticket</a>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>

        </div>
@endsection
