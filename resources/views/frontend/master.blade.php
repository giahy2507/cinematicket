<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html lang="en-US" ng-app="CinematicketApp">
<head>
    <title>@yield('title')</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href='https://fonts.googleapis.com/css?family=Maven+Pro:400,900,700,500' rel='stylesheet' type='text/css'>

    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
    <!--- start-mmmenu-script---->
    <script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
    <link type="text/css" rel="stylesheet" href="{{ asset('css/jquery.mmenu.all.css') }}" />
    <script type="text/javascript" src="{{ asset('js/jquery.mmenu.js') }}"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <script src="https://code.angularjs.org/1.2.25/angular-route.js"></script>
    <!-- start top_js_button -->
    <script type="text/javascript" src="{{ asset('js/easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/move-top.js') }}"></script>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
            });
        });
    </script>
    <script type="text/javascript">
        //	The menu on the left
        $(function() {
            $('nav#menu-left').mmenu();
        });
        var CinematicketApp = angular.module('CinematicketApp', ['UserService', 'MovieService','CinemaService','ReservationService'], function($interpolateProvider) {
            $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');
        });

        CinematicketApp.controller('PhoneListCtrl', function ($scope) {
            $scope.phones = [
                {'name': 'Nexus S',
                    'snippet': 'Fast just got faster with Nexus S.'},
                {'name': 'Motorola XOOM™ with Wi-Fi',
                    'snippet': 'The Next, Next Generation tablet.'},
                {'name': 'MOTOROLA XOOM™',
                    'snippet': 'The Next, Next Generation tablet.'}
            ];
        });

    </script>
    <!-- Angular User Script -->
    <script src="{{ asset('js/angularjs/UserService.js') }}"></script>
    <script src="{{ asset('js/angularjs/MovieService.js') }}"></script>
    <script src="{{ asset('js/angularjs/CinemaService.js') }}"></script>
    <script src="{{ asset('js/angularjs/ReservationService.js') }}"></script>
    <script src="{{ asset('js/angularjs/UserController.js') }}"></script>
    <script src="{{ asset('js/angularjs/HeaderController.js') }}"></script>

    @yield('header')
</head>
<body>
    <div id="header_container" ng-controller = "HeaderController">
    <!-- start header -->
    <div class="top_bg">
        <div class="wrap">
            <div class="header">
                <div class="logo">
                    <a href="{{ URL::to('/')}}"><img src="{{ asset('images/logo.png') }}" alt=""/></a>
                </div>
                <div class="log_reg">
                    <ul>
                        <span id="login-area-userbtn"><a href="{{ URL::to('/user')}}" style="text-transform: uppercase"><%user_name%></a></span>
                        <span id="login-area-logoutbtn" class="log"><a href="#" ng-click="logoutRequest()" style="text-transform: capitalize">log out</a></span>
                        <span id="login-area-loginbtn"><a href="{{ URL::to('/login')}}">Log in</a></span>
                        <span id="login-area-registerbtn"><a href="{{ URL::to('/register')}}">Register</a></span>
                        <div class="clear"></div>
                        <script>
                            function change_login_area() {
                                var userbtn = document.getElementById('login-area-userbtn');
                                var logoutbtn = document.getElementById('login-area-logoutbtn');
                                var loginbtn = document.getElementById('login-area-loginbtn');
                                var registerbtn = document.getElementById('login-area-registerbtn');
                                var access_token =  localStorage.access_token;
                                if(access_token === "" || access_token==null){
                                    console.log("0");
                                    userbtn.style.display = 'none';
                                    logoutbtn.style.display = 'none';
                                    loginbtn.style.display = 'inline-block';
                                    registerbtn.style.display = 'inline-block';
                                }else{
                                    console.log("1");
                                    userbtn.style.display = 'inline-block';
                                    logoutbtn.style.display = 'inline-block';
                                    loginbtn.style.display = 'none';
                                    registerbtn.style.display = 'none';
                                }
                            }
                            change_login_area();
                        </script>
                    </ul>
                </div>
                <div class="web_search">
                    <form action="/search" method="get">
                        <input type="text" name="search_query" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}">
                        <input type="submit" value="" />
                    </form>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <!-- start header_btm -->
    <div class="wrap">
        <div class="header_btm">
            <div class="menu">
                <ul>
                    <li class="active"><a href="{{ URL::to('/')}}">Home</a></li>
                    <li><a href="{{ URL::to('/nowshowing')}}">Now Showing</a></li>
                    <li><a href="{{ URL::to('/comingsoon')}}">Coming Soon</a></li>
                    <li><a href="{{ URL::to('/cinema')}}">Cinema</a></li>
                    <div class="clear"></div>
                </ul>
            </div>
            <div id="smart_nav">
                <a class="navicon" href="#menu-left"> </a>
            </div>
            <nav id="menu-left">
                <ul>
                    <li class="active"><a href="{{ URL::to('/')}}">Home</a></li>
                    <li><a href="{{ URL::to('/nowshowing')}}">Now Showing</a></li>
                    <li><a href="{{ URL::to('/comingsoon')}}">Coming Soon</a></li>
                    <li><a href="{{ URL::to('/cinema')}}">Cinema</a></li>
                    <div class="clear"></div>
                </ul>
            </nav>
            <div class="header_right">
                <ul>
                    <li><a href="#"><i  class="art"></i><span class="color1">30</span></a></li>
                    <li><a href="#"><i  class="cart"></i><span>0</span></a></li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    </div>

<div class="container">
    @yield('content')
</div>

<!-- start footer -->
<div class="footer_top">
    <div class="wrap">
        <div class="footer">
            <!-- start grids_of_3 -->
            <div class="span_of_3">
                <div class="span1_of_3">
                    <h3>text edit</h3>
                    <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>
                </div>
                <div class="span1_of_3">
                    <h3>twitter widget</h3>
                    <p><a href="#">@Contrarypopular</a> I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give <a href="#">@accountofsystem</a> </p>
                    <p class="top">19 days ago</p>
                    <p class="top"><a href="#">@Contrarypopular</a> I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give <a href="#">@accountofsystem</a> </p>
                    <p class="top">19 days ago</p>
                </div>
                <div class="span1_of_3">
                    <h3>flickr widget</h3>
                    <ul class="f_nav">
                        <li><a href="#"><img src="{{ asset('images/f_pic1.jpg') }}" alt="" /> </a></li>
                        <li><a href="#"><img src="{{ asset('images/f_pic2.jpg') }}" alt="" /> </a></li>
                        <li><a href="#"><img src="{{ asset('images/f_pic3.jpg') }}" alt="" /> </a></li>
                        <li><a href="#"><img src="{{ asset('images/f_pic4.jpg') }}" alt="" /> </a></li>
                        <li><a href="#"><img src="{{ asset('images/f_pic5.jpg') }}" alt="" /> </a></li>
                        <li><a href="#"><img src="{{ asset('images/f_pic6.jpg') }}" alt="" /> </a></li>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>
<!-- start footer -->
<div class="footer_mid">
    <div class="wrap">
        <div class="footer">
            <div class="f_search">
                <form>
                    <input type="text" value="" placeholder="Enter email for newsletter" />
                    <input type="submit" value=""/>
                </form>
            </div>
            <div class="soc_icons">
                <ul>
                    <li><a class="icon1" href="#"></a></li>
                    <li><a class="icon2" href="#"></a></li>
                    <li><a class="icon3" href="#"></a></li>
                    <li><a class="icon4" href="#"></a></li>
                    <li><a class="icon5" href="#"></a></li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!-- start footer -->
<div class="footer_bg">
    <div class="wrap">
        <div class="footer">
            <!-- scroll_top_btn -->
            <script type="text/javascript">
                $(document).ready(function() {

                    var defaults = {
                        containerID: 'toTop', // fading element id
                        containerHoverID: 'toTopHover', // fading element hover id
                        scrollSpeed: 1200,
                        easingType: 'linear'
                    };


                    $().UItoTop({ easingType: 'easeOutQuart' });

                });
            </script>
            <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
            <!--end scroll_top_btn -->
            <div class="f_nav1">
                <ul>
                    <li><a href="#">home /</a></li>
                    <li><a href="#">support /</a></li>
                    <li><a href="#">Terms and conditions /</a></li>
                    <li><a href="#">Faqs /</a></li>
                    <li><a href="#">Contact us</a></li>
                </ul>
            </div>
            <div class="copy">
                <p class="link"><span>© All rights reserved | Template by&nbsp;<a href="https://w3layouts.com/"> W3Layouts</a></span></p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</body>
</html>