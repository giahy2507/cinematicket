@extends('frontend.master')

@section('title', 'Cinema Ticket now Showing')

@section('header')

<script src="{{ asset('js/angularjs/MovieController.js') }}" ></script>

@endsection

@section('content')
    <div id="comingsoon_container" ng-controller="MovieController">
        <script>
            angular.element(document.getElementById('comingsoon_container')).scope().get_movies_request();
        </script>
        <!-- start main -->
        <div class="main_bg">
            <div class="wrap">
                <div class="main">
                    <div class="top_main">
                        <h2>Coming soon</h2>
                        <div class="clear"></div>
                    </div>
                    <!-- start grids_of_3 -->
                    <div class="grids_of_3">
                        <div class="grid1_of_3" ng-repeat="movie in movies_comingsoon">
                            <a href="<% base_url + 'movie/' + movie.id %>">
                                <img src="<%base_url + movie.poster_url%>"/>
                                <h3><%movie.name%></h3>
                            </a>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                    <!-- start grids_of_2 -->
                    <div class="grids_of_2">
                        <div class="grid1_of_2">
                            <div class="span1_of_2">
                                <h2>free shipping</h2>
                                <p>Lorem Ipsum is simply dummy  typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                            <div class="span1_of_2">
                                <h2>testimonials</h2>
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using [...]</p>
                            </div>
                        </div>
                        <div class="grid1_of_2 bg">
                            <h2>blog news</h2>
                            <div class="grid_date">
                                <div class="date1">
                                    <h4>apr 01</h4>
                                </div>
                                <div class="date_text">
                                    <h4><a href="#"> Donec vehicula est ac augue consectetur,</a></h4>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</p>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="grid_date">
                                <div class="date1">
                                    <h4>feb 01</h4>
                                </div>
                                <div class="date_text">
                                    <h4><a href="#"> The standard chunk of Lorem Ipsum used since ,,</a></h4>
                                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from </p>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection