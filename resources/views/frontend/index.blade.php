@extends('frontend.master')

@section('title', 'Cinema Ticket')



@section('header')
<!-- start slider -->
<link href="{{ asset('css/slider.css') }}" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="{{ asset('js/jquery.eislideshow.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/easing.js') }}"></script>
<script type="text/javascript">
    $(function() {
        $('#ei-slider').eislideshow({
            animation			: 'center',
            autoplay			: true,
            slideshow_interval	: 3000,
            titlesFactor		: 0
        });
    });
</script>
<!-- start top_js_button -->
<script type="text/javascript" src="{{ asset('js/move-top.js') }}"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
        });
    });
</script>


<script src="{{ asset('js/angularjs/MovieController.js') }}" ></script>

@endsection

@section('content')
        <div id="index_container" ng-controller="MovieController">
            <script>
                angular.element(document.getElementById('index_container')).scope().get_movies_request();
            </script>
            <!-- start slider -->
            <div class="slider">
                <!---start-image-slider---->
                <div class="image-slider">
                    <div class="wrapper">
                        <div id="ei-slider" class="ei-slider">
                            <ul class="ei-slider-large">
                                <li>
                                    <a href="#">
                                        <img src="images/slider/danish.jpg"/>
                                    </a>
                                </li>

                                <li><a href="#">
                                        <img src="images/slider/point_break.jpg"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="images/slider/snoopy.jpg"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="images/slider/starwars.jpg"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="images/slider/little-prince.jpg"/>
                                    </a>
                                </li>

                            </ul><!-- ei-slider-large -->
                            <ul class="ei-slider-thumbs">
                                <li class="ei-slider-element">Current</li>
                                <li>
                                    <a href="#">
                                        <span class="active">The Danish Girl</span>
                                        <p>Now Showing</p>
                                    </a>
                                    <img src="images/slider/danish.jpg" alt="thumb01" />
                                </li>
                                <li  class="hide1"><a href="#"><span>Point Break</span><p>Now Showing</p></a><img src="images/slider/point_break.jpg" alt="thumb02" /></li>
                                <li class="hide1"><a href="#"><span>Snoopy: The Peanuts Movie</span><p>Now Showing</p> </a><img src="images/slider/snoopy.jpg" alt="thumb03" /></li>
                                <li><a href="#"><span>Star Wars</span><p>Now Showing</p> </a><img src="images/slider/starwars.jpg" alt="thumb04" /></li>
                                <li class="hide"><a href="#"><span>The Little Prince</span><p>Coming Soon</p> </a><img src="images/slider/little-prince.jpg" alt="thumb01" /></li>

                            </ul><!-- ei-slider-thumbs -->
                        </div><!-- ei-slider -->
                    </div><!-- wrapper -->
                </div>
                <!---End-image-slider---->
            </div>
            <!-- start image1_of_3 -->
            <div class="top_bg">
                <div class="wrap">
                    <div class="main1">
                        <div class="image1_of_3">
                            <img src="/images/popcorn.jpg" alt=""/>
                            <a href="#"><span class="tag">Service</span></a>
                        </div>
                        <div class="image1_of_3">
                            <img src="/images/likefacebook.jpg" alt=""/>
                            <a href="#"><span class="tag">Follow us</span></a>
                        </div>
                        <div class="image1_of_3">
                            <img src="/images/mobileapp.jpg" alt=""/>
                            <a href="#"><span class="tag">Mobile App</span></a>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>

            <!-- start main -->
            <div class="main_bg">
                <div class="wrap">
                    <div class="main">
                        <div class="top_main">
                            <h2>Now Showing</h2>
                            <a href="/nowshowing">show all</a>
                            <div class="clear"></div>
                        </div>
                        <!-- start grids_of_3 -->
                        <div class="grids_of_3">
                            <div class="grid1_of_3" ng-repeat="movie in movies_nowshowing">
                                <a href="<% base_url + 'movie/' + movie.id %>">
                                    <img src="<%base_url + movie.poster_url%>"/>
                                    <h3><%movie.name%></h3>
                                </a>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="top_main">
                            <h2>Coming Soon</h2>
                            <a href="/comingsoon">show all</a>
                            <div class="clear"></div>
                        </div>
                        <!-- start grids_of_3 -->
                        <div class="grids_of_3">
                            <div class="grid1_of_3" ng-repeat="movie in movies_comingsoon">
                                <a href="<% base_url + 'movie/' + movie.id %>">
                                    <img src="<%base_url + movie.poster_url%>"/>
                                    <h3><%movie.name%></h3>
                                </a>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
@endsection