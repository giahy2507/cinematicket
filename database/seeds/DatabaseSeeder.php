<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(StarSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(MovieSeeder::class);
        $this->call(Star_Movie_Role_Seeder::class);
        $this->call(CinemaSeeder::class);
        $this->call(ShowtimeSeeder::class);
        $this->call(Seat_Seeder::class);
        $this->call(ReservationSeeder::class);
    }
}


class StarSeeder extends Seeder
{
    public function run()
    {
        DB::table('stars')->delete();
        DB::table('stars')->insert([
            ['id'=> 1 ,  'name' => 'Teresa Palmer', 'birthday' => '1986-02-26', 'gender' => 'Female', 'nationality' => 'Australia', 'deathday' => ''],
            ['id'=> 2 ,'name' => 'Luke Bracey', 'birthday' => '1989-04-26', 'gender' => 'Male', 'nationality' => 'America', 'deathday' => ''],
            ['id'=> 3 ,'name' => 'Édgar Ramírez', 'birthday' => '1977-03-25', 'gender' => 'Male', 'nationality' => 'Venezuela', 'deathday' => ''],
            ['id'=> 4 ,'name' => 'Rachel McAdams', 'birthday' => '1978-11-17', 'gender' => 'Female', 'nationality' => 'Canada', 'deathday' => ''],
            ['id'=> 5 ,'name' => 'Eddie Redmayne', 'birthday' => '1982-01-06', 'gender' => 'Male', 'nationality' => 'England', 'deathday' => ''],
            ['id'=> 6 ,'name' => 'Alicia Vikander', 'birthday' => '1988-10-03', 'gender' => 'Female', 'nationality' => 'Sweden', 'deathday' => ''],
            ['id'=> 7 ,'name' => 'Michael Fassbender', 'birthday' => '1977-02-04', 'gender' => 'Male', 'nationality' => 'America', 'deathday' => ''],
            ['id'=> 8 ,'name' => 'Amber Heard', 'birthday' => '1986-04-22', 'gender' => 'Female', 'nationality' => 'America', 'deathday' => ''],
            ['id'=> 9 ,'name' => 'Chloë Grace Moretz', 'birthday' => '1997-02-10', 'gender' => 'Female', 'nationality' => 'America', 'deathday' => ''],
        ]);
    }
}
class UserSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            ['id'=> 1 ,'user_name' => 'giahy', 'password' => '123', 'admin' => true , 'name' => 'giahy', 'email' => 'giahy0257@gmail.com', 'phone_number' => '123456789', 'membership' => 'gold'],
        ]);
        DB::table('users')->insert([
            ['id'=> 2 ,'user_name' => 'hynguyen', 'password' => '123', 'name' => 'hynguyen', 'email' => 'giahy2507@gmail.com', 'phone_number' => '987654321', 'membership' => 'gold'],
        ]);
        DB::table('users')->insert([
            ['id'=> 3 ,'user_name' => 'thy', 'password' => '123', 'name' => 'thythy', 'email' => 'thy2512@gmail.com', 'phone_number' => '000000000', 'membership' => 'blue'],
        ]);
        DB::table('users')->insert([
            ['id'=> 4 ,'user_name' => 'thythy', 'password' => '123', 'name' => 'banhbeo', 'email' => 'banhbeo@gmail.com', 'phone_number' => '000000000', 'membership' => 'platinum'],
        ]);
    }
}

class MovieSeeder extends Seeder
{
    public function run()
    {
        DB::table('movies')->delete();
        DB::table('movies')->insert([
            ['id'=> 1 ,'name' => 'The Danish Girl', 'cover_url' => 'images/slider/danish.jpg', 'thumbnail_url' => 'images/thumbs/danish.jpg', 'poster_url' => 'images/small/danish.jpg', 'release_date' => '2016-01-15', 'genres' => 'biography, drama'],
            ['id'=> 2 ,'name' => 'The Little Prince', 'cover_url' => 'images/slider/little-prince.jpg', 'thumbnail_url' => 'images/thumbs/little-prince.jpg', 'poster_url' => 'images/small/little-prince.jpg', 'release_date' => '2016-03-18', 'genres' => 'animation, fantasy'],
            ['id'=> 3 ,'name' => 'Point Break', 'cover_url' => 'images/slider/point_break.jpg', 'thumbnail_url' => 'images/thumbs/point_break.jpg', 'poster_url' => 'images/small/point_break.jpg', 'release_date' => '2015-12-25', 'genres' => 'action'],
            ['id'=> 4 ,'name' => 'Snoopy: The Peanuts Movie', 'cover_url' => 'images/slider/snoopy.jpg', 'thumbnail_url' => 'images/thumbs/snoopy.jpg', 'poster_url' => 'images/small/snoopy.jpg', 'release_date' => '2015-11-06', 'genres' => 'comedy, animation'],
            ['id'=> 5 ,'name' => 'Star Wars', 'cover_url' => 'images/slider/starwars.jpg', 'thumbnail_url' => 'images/thumbs/starwars.jpg', 'poster_url' => 'images/small/starwars.jpg', 'release_date' => '2015-12-18', 'genres' => 'action, adventure, fantasy'],
            ['id'=> 6 ,'name' => 'Steve Jobs', 'cover_url' => 'images/slider/stevejobs.jpg', 'thumbnail_url' => 'images/thumbs/stevejobs.jpg', 'poster_url' => 'images/small/stevejobs.jpg', 'release_date' => '2015-10-23', 'genres' => 'biography, drama'],
            ['id'=> 7 ,'name' => 'The 5th Wave', 'cover_url' => 'images/slider/the-5th-wave.jpg', 'thumbnail_url' => 'images/thumbs/the-5th-wave.jpg', 'poster_url' => 'images/small/the-5th-wave.jpg', 'release_date' => '2016-01-22', 'genres' => 'adventure, sci-fi, thriller'],
        ]);
    }
}

class Star_Movie_Role_Seeder extends Seeder
{
    public function run()
    {
        DB::table('star_movie_roles')->delete();
        DB::table('star_movie_roles')->insert([
            ['id'=> 1 ,'role' => '', 'star_id' => '1', 'movie_id' => '3'],
            ['id'=> 2 ,'role' => '', 'star_id' => '5', 'movie_id' => '1'],
            ['id'=> 3 ,'role' => '', 'star_id' => '6', 'movie_id' => '1'],
            ['id'=> 4 ,'role' => '', 'star_id' => '8', 'movie_id' => '1'],
            ['id'=> 5 ,'role' => '', 'star_id' => '9', 'movie_id' => '7'],
            ['id'=> 6 ,'role' => '', 'star_id' => '7', 'movie_id' => '6'],
            ['id'=> 7 ,'role' => '', 'star_id' => '4', 'movie_id' => '2'],
            ['id'=> 8 ,'role' => '', 'star_id' => '2', 'movie_id' => '4'],
            ['id'=> 9 ,'role' => '', 'star_id' => '3', 'movie_id' => '3'],
        ]);
    }
}

class CinemaSeeder extends Seeder
{
    public function run()
    {
        DB::table('cinemas')->delete();
        DB::table('cinemas')->insert([
            ['id'=> 1 ,'name' => 'Galaxy Nguyen Du', 'address' => '116 Nguyen Du, Quan 1, Tp.HCM', 'phone_number'=> '(08) 3823 5235'],
            ['id'=> 2 ,'name' => 'Galaxy Nguyen Trai', 'address' => '230 Nguyen Trai, Quan 1, Tp.HCM', 'phone_number'=> '(08) 3920 6688'],
            ['id'=> 3 ,'name' => 'CGV Hung Vuong Plaza', 'address' => 'Tang 7 | Hung Vuong Plaza 126 Hung Vuong, Quan 5, Tp.HCM', 'phone_number'=> '+84 8 2 222 0388'],
            ['id'=> 4 ,'name' => 'CGV Crescent Mall', 'address' => 'Lau 5, Crescent Mall Nguyen Van Linh, Phu My Hung, Quan 7, Tp.HCM', 'phone_number'=> '+848 5 412 2222'],
        ]);
    }
}

class ShowtimeSeeder extends Seeder
{
    public function run()
    {
        DB::table('showtimes')->delete();
        DB::table('showtimes')->insert([
            ['id'=> 1 ,'type' => 'sub', 'time' => '2015-12-29 13:00:00', 'movie_id' => '3', 'cinema_id' => '1'],
            ['id'=> 2 ,'type' => 'sub', 'time' => '2015-12-30 13:00:00', 'movie_id' => '4', 'cinema_id' => '3'],
            ['id'=> 3 ,'type' => 'sub', 'time' => '2015-12-29 09:40:00', 'movie_id' => '5', 'cinema_id' => '2'],
            ['id'=> 4 ,'type' => 'sub', 'time' => '2016-01-15 19:10:00', 'movie_id' => '1', 'cinema_id' => '3'],
        ]);
    }
}

class Seat_Seeder extends Seeder
{
    public function run()
    {
        DB::table('seats')->delete();
        DB::table('seats')->insert([
            ['id'=> 1 ,'seat_number' => '0', 'cinema_id' => '3'],
            ['id'=> 2 ,'seat_number' => '6', 'cinema_id' => '3'],
            ['id'=> 3 ,'seat_number' => '4', 'cinema_id' => '3'],
            ['id'=> 4 ,'seat_number' => '7', 'cinema_id' => '2'],

            ['id'=> 5 ,'seat_number' => '0', 'cinema_id' => '1'],
            ['id'=> 6 ,'seat_number' => '2', 'cinema_id' => '1'],
            ['id'=> 7 ,'seat_number' => '1', 'cinema_id' => '1'],
            ['id'=> 8 ,'seat_number' => '4', 'cinema_id' => '1'],
            ['id'=> 9 ,'seat_number' => '3', 'cinema_id' => '1'],

            ['id'=> 10 ,'seat_number' => '3', 'cinema_id' => '4'],
            ['id'=> 11,'seat_number' => '1', 'cinema_id' => '2'],
            ['id'=> 12 ,'seat_number' => '5', 'cinema_id' => '4'],
        ]);
    }
}

class ReservationSeeder extends Seeder
{
    public function run()
    {
        DB::table('reservation')->delete();
        DB::table('reservation')->insert([
            ['id'=> 1 ,'date' => 'null', 'showtime_id' => '1', 'user_id' => '1', 'seat_id' => '8'],
            ['id'=> 2 ,'date' => '2015-12-29', 'showtime_id' => '2', 'user_id' => '2', 'seat_id' => '1'],
            ['id'=> 3 ,'date' => '2015-12-30', 'showtime_id' => '3', 'user_id' => '4', 'seat_id' => '4'],
            ['id'=> 4 ,'date' => '2015-03-03', 'showtime_id' => '4', 'user_id' => '3', 'seat_id' => '2'],
        ]);
    }
}