<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShowtimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('showtimes', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['sub', 'dub']);
            $table->datetime('time');
            $table->timestamps();
            $table->integer('movie_id')->unsigned();
            $table->integer('cinema_id')->unsigned();
            $table->boolean('deleted')->default(false);
        });

        Schema::table('showtimes', function(Blueprint $table){
            $table->foreign('movie_id')->references('id')->on('movies')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('cinema_id')->references('id')->on('cinemas')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('showtimes', function(Blueprint $table) {
            $table->dropForeign('showtimes_movie_id_foreign');
            $table->dropForeign('showtimes_cinema_id_foreign');
        });

        Schema::drop('showtimes');
    }
}
