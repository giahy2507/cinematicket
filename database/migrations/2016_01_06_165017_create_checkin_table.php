<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkin', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reservation_id')->unsigned();
            $table->boolean('deleted')->default(false);
            $table->timestamps();
        });

        Schema::table('checkin', function(Blueprint $table){
            $table->foreign('reservation_id')->references('id')->on('reservation')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('checkin', function(Blueprint $table) {
            $table->dropForeign('checkin_reservation_id_foreign');
        });
        Schema::drop('checkin');
    }
}
