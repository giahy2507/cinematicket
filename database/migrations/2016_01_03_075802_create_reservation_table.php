<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('reservation', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('showtime_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('seat_id')->unsigned();
            $table->boolean('deleted')->default(false);
            $table->timestamps();
        });

        Schema::table('reservation', function(Blueprint $table){
            $table->foreign('showtime_id')->references('id')->on('showtimes')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('seat_id')->references('id')->on('seats')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('reservation', function(Blueprint $table) {
            $table->dropForeign('reservation_showtime_id_foreign');
            $table->dropForeign('reservation_user_id_foreign');
            $table->dropForeign('reservation_seat_id_foreign');
        });

        Schema::drop('reservation');
    }
}
