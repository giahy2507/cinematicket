<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStarMovieRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('star_movie_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('role');
            $table->timestamps();
            $table->integer('star_id')->unsigned();
            $table->integer('movie_id')->unsigned();
            $table->boolean('deleted')->default(false);
        });
        
        Schema::table('star_movie_roles', function(Blueprint $table){
            $table->foreign('star_id')->references('id')->on('stars')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('movie_id')->references('id')->on('movies')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('star_movie_roles', function(Blueprint $table) {
            $table->dropForeign('star_movie_roles_star_id_foreign');
            $table->dropForeign('star_movie_roles_movie_id_foreign');
        });

        Schema::drop('star_movie_roles');
    }
}
