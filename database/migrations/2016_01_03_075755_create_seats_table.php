<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('seats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seat_number')->unsigned; // 0->9
            $table->boolean('deleted')->default(false);
            $table->timestamps();
            $table->integer('cinema_id')->unsigned();
        });

        Schema::table('seats', function(Blueprint $table){
            $table->foreign('cinema_id')->references('id')->on('cinemas')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('seats', function(Blueprint $table) {
            $table->dropForeign('seats_cinema_id_foreign');
        });
        Schema::drop('seats');
    }
}
