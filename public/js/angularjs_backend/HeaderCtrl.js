angular.module('adminApp').controller('HeaderCtrl', function($scope, $http, UserSv) {
    
    $scope.user_name = localStorage.user_name;
    $scope.admin = localStorage.admin;

    $scope.logoutRequest = function(){
        console.log("angular logout request");
        UserSv.logout()
            .success(function (data, status) {
                $scope.user_data = {};
                if (status == 203) {
                } else if (status == 200) {
                    console.log("logout thanh cong");
                    localStorage.setItem("admin_access_token","");
                }
                localStorage.setItem('admin', "");
                window.location.replace(window.location.origin + '/admin');
            })
            .error(function (data) {
                console.log(data);
            });
    };

});