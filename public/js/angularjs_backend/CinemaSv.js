angular.module('CinemaSv', []).factory('CinemaSv', function($http) {
    var access_token = localStorage.admin_access_token;
    return {
        // get all Cinemas
        get : function() {
            console.log("CinemaSv get function");
            return $http({
                url: window.location.origin +'/api/cinema',
                method: "GET",
                params: {}
            });
        },
        show : function(id) {
            console.log("CinemaSv get function");
            return $http({
                url: window.location.origin +'/api/cinema/' + id,
                method: "GET",
                params: {}
            });
        },
        destroy : function(id) {
            console.log("CinemaSv delete function")
            return $http({
                url: window.location.origin + '/api/cinema/' + id,
                method: "DELETE",
                params: {access_token:access_token}
            });
        },

        store : function(data) {
            console.log("CinemaSv store");
            console.log(data);
            return $http({
                    method: 'POST',
                    url: window.location.origin + '/api/cinema',
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    params: {access_token:access_token},
                    data: $.param(data)
                });

        },

        update : function(data) {
            console.log("CinemaSv update");
            return $http({
                    method: 'PUT',
                    url: window.location.origin + '/api/cinema/' + data.id,
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    params: {access_token:access_token},
                    data: $.param(data)
                });
        },
    };
});