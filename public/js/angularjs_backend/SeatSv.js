angular.module('SeatSv', []).factory('SeatSv', function($http) {
    var access_token = localStorage.admin_access_token;
    return {
        // get all Seats
        get : function() {
            console.log("SeatSv get function");
            return $http({
                url: window.location.origin +'/api/seat',
                method: "GET",
                params: {}
            });
        },
        show : function(id) {
            console.log("SeatSv get function");
            return $http({
                url: window.location.origin +'/api/seat/' + id,
                method: "GET",
                params: {}
            });
        },
        destroy : function(id) {
            console.log("SeatSv delete function")
            return $http({
                url: window.location.origin + '/api/seat/' + id,
                method: "DELETE",
                params: {access_token:access_token}
            });
        },

        store : function(data) {
            console.log("SeatSv store");
            console.log(data);
            return $http({
                    method: 'POST',
                    url: window.location.origin + '/api/seat',
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    params: {access_token:access_token},
                    data: $.param(data)
                });

        },

        update : function(data) {
            console.log("SeatSv update");
            return $http({
                    method: 'PUT',
                    url: window.location.origin + '/api/seat/' + data.id,
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    params: {access_token:access_token},
                    data: $.param(data)
                });
        },
    };
});