console.log("loaded StarCtrl.js");

var CinematicketApp = angular.module('adminApp');

CinematicketApp.controller('StarCtrl', function($scope, StarSv) {
    if (!localStorage.admin){
        var url = "http://" + window.location.host + "/admin";
        console.log(url);
        window.location.href = url;
    }
    // object to hold all the star data
    $scope.stars = [];
    $scope.query = "";

    StarSv.get()
        .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.stars = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });

    $scope.set_query = function(query){
        $scope.query= query
    };

    $scope.delete = function(id){
        console.log("delete star");
        $scope.stars = {};
        StarSv.destroy(id.toString())
            .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.stars = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });
    };

    $scope.add = function(star){
        console.log("add star");
        console.log("star");
        $scope.stars = {};

        if ($scope.btn_name == "Add") {
            StarSv.store(star)
                .success(function (data, status) {
                    if (status == 203) {
                        window.alert("Not authenticate");
                    } else if (status == 200) {
                        $scope.stars = data;
                    }
                })
                .error(function (data) {
                    console.log("404 not found - blank access_token");
                });
        }
        else {
            StarSv.update(star)
                .success(function (data, status) {
                    if (status == 203) {
                        window.alert("Not authenticate");
                    } else if (status == 200) {
                        $scope.stars = data;
                    }
                })
                .error(function (data) {
                    console.log("404 not found - blank access_token");
                });
        }

        $scope.showModal = !$scope.showModal;
    };

    $scope.showModal = false;
    $scope.editModal = function(data){
        $scope.showModal = !$scope.showModal;
        $scope.star = data;
        $scope.btn_name = "Save";
    };

    $scope.addNewModal = function(){
        $scope.showModal = !$scope.showModal;
        $scope.star = {};
        $scope.btn_name = "Add";
    };

});