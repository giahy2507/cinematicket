console.log("loaded ShowtimeCtrl.js");

var CinematicketApp = angular.module('adminApp');

CinematicketApp.controller('ShowtimeCtrl', function($scope, ShowtimeSv, MovieSv, CinemaSv) {
    if (!localStorage.admin){
        var url = "http://" + window.location.host + "/admin";
        console.log(url);
        window.location.href = url;
    }
    // object to hold all the showtime data
    $scope.showtimes = [];
    $scope.query = "";

    ShowtimeSv.get()
        .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.showtimes = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });

    $scope.movies = [];
    $scope.selectedMovie = "";
    MovieSv.get()
        .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.movies = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });


    $scope.cinemas = [];
    $scope.selectedCinema = "";

    CinemaSv.get()
        .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.cinemas = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });

    $scope.changedMovie =function(item){
        $scope.selectedMovie = item;
    }
    $scope.changedCinema =function(item){
        $scope.selectedCinema = item;
    }

    $scope.set_query = function(query){
        $scope.query= query
    };

    $scope.delete = function(id){
        console.log("delete showtime");
        $scope.showtimes = {};
        ShowtimeSv.destroy(id.toString())
            .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.showtimes = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });
    };

    $scope.add = function(showtime){
        $scope.showtimes = {};

        showtime.movie_id = $scope.selectedMovie.id;
        showtime.cinema_id = $scope.selectedCinema.id;

        if ($scope.btn_name == "Add") {
            ShowtimeSv.store(showtime)
                .success(function (data, status) {
                    if (status == 203) {
                        window.alert("Not authenticate");
                    } else if (status == 200) {
                        $scope.showtimes = data;
                    }
                })
                .error(function (data) {
                    console.log("404 not found - blank access_token");
                });
        }
        else {
            ShowtimeSv.update(showtime)
                .success(function (data, status) {
                    if (status == 203) {
                        window.alert("Not authenticate");
                    } else if (status == 200) {
                        $scope.showtimes = data;
                    }
                })
                .error(function (data) {
                    console.log("404 not found - blank access_token");
                });
        }

        $scope.showModal = !$scope.showModal;
    };

    $scope.showModal = false;
    $scope.editModal = function(data){
        $scope.showModal = !$scope.showModal;
        $scope.showtime = data;
        $scope.btn_name = "Save";
        $scope.movies.forEach(function(entry) {
            if (entry.id == data.movie_id){
                $scope.selectedMovie = entry;
            }
        });
        $scope.cinemas.forEach(function(entry) {
            if (entry.id == data.cinema_id){
                $scope.selectedCinema = entry;
            }
        });
    };

    $scope.addNewModal = function(){
        $scope.showModal = !$scope.showModal;
        $scope.showtime = {};
        $scope.btn_name = "Add";
        $scope.selectedMovie = "";
        $scope.selectedCinema = "";
    };

});