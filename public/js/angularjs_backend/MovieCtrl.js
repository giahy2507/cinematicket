console.log("loaded MovieCtrl.js");

var CinematicketApp = angular.module('adminApp');

CinematicketApp.controller('MovieCtrl', function($scope, MovieSv) {
    if (!localStorage.admin){
        var url = "http://" + window.location.host + "/admin";
        console.log(url);
        window.location.href = url;
    }
    // object to hold all the movie data
    $scope.movies = [];
    $scope.query = "";

    MovieSv.get()
        .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.movies = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });

    $scope.set_query = function(query){
        $scope.query= query
    };

    $scope.delete = function(id){
        console.log("delete movie");
        $scope.movies = {};
        MovieSv.destroy(id.toString())
            .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.movies = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });
    };

    $scope.add = function(movie){
        console.log("add movie");
        console.log("movie");
        $scope.movies = {};

        if ($scope.btn_name == "Add") {
            MovieSv.store(movie)
                .success(function (data, status) {
                    if (status == 203) {
                        window.alert("Not authenticate");
                    } else if (status == 200) {
                        $scope.movies = data;
                    }
                })
                .error(function (data) {
                    console.log("404 not found - blank access_token");
                });
        }
        else {
            MovieSv.update(movie)
                .success(function (data, status) {
                    if (status == 203) {
                        window.alert("Not authenticate");
                    } else if (status == 200) {
                        $scope.movies = data;
                    }
                })
                .error(function (data) {
                    console.log("404 not found - blank access_token");
                });
        }

        $scope.showModal = !$scope.showModal;
    };

    $scope.showModal = false;
    $scope.editModal = function(data){
        $scope.showModal = !$scope.showModal;
        $scope.movie = data;
        $scope.btn_name = "Save";
    };

    $scope.addNewModal = function(){
        $scope.showModal = !$scope.showModal;
        $scope.movie = {};
        $scope.btn_name = "Add";
    };

});

CinematicketApp.directive('modal', function () {
    return {
      template: '<div class="modal fade">' + 
          '<div class="modal-dialog">' + 
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                '<h4 class="modal-title">{{ title }}</h4>' + 
              '</div>' + 
              '<div class="modal-body" ng-transclude></div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
        restrict: 'E',
        transclude: true,
        replace:true,
        scope:true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;

            scope.$watch(attrs.visible, function(value){
                if(value == true)
                    $(element).modal('show');
                else
                    $(element).modal('hide');
            });

            $(element).on('shown.bs.modal', function(){
                scope.$apply(function(){
                scope.$parent[attrs.visible] = true;
              });
            });

            $(element).on('hidden.bs.modal', function(){
                scope.$apply(function(){
                scope.$parent[attrs.visible] = false;
            });
        });
      }
    };
  });

