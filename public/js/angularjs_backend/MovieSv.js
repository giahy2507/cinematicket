angular.module('MovieSv', []).factory('MovieSv', function($http) {
    var access_token = localStorage.admin_access_token;
    return {
        // get all movies
        get : function() {
            console.log("MovieSv get function");
            return $http({
                url: window.location.origin +'/api/movie',
                method: "GET",
                params: {}
            });
        },
        show : function(id) {
            console.log("MovieSv get function");
            return $http({
                url: window.location.origin +'/api/movie/' + id,
                method: "GET",
                params: {}
            });
        },
        destroy : function(id) {
            console.log("MovieSv delete function")
            return $http({
                url: window.location.origin + '/api/movie/' + id,
                method: "DELETE",
                params: {access_token:access_token}
            });
        },

        store : function(data) {
            console.log("MovieSv store");
            console.log(data);
            return $http({
                    method: 'POST',
                    url: window.location.origin + '/api/movie',
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    params: {access_token:access_token},
                    data: $.param(data)
                });

        },

        update : function(data) {
            console.log("MovieSv update");
            return $http({
                    method: 'PUT',
                    url: window.location.origin + '/api/movie/' + data.id,
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    params: {access_token:access_token},
                    data: $.param(data)
                });
        },
    };
});