angular.module('UserSv', []).factory('UserSv', function($http) {

    var access_token = localStorage.admin_access_token;
        return {
            login: function (user_data) {
                return $http({
                    method: 'POST',
                    url: window.location.origin + '/api/admin/login',
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    data: $.param(user_data)
                });
            },

            logout : function() {
                return $http({
                    url: window.location.origin +'/api/admin/logout',
                    method: "GET",
                    params: {access_token:access_token}
                });
            },

            get : function() {
                return $http({
                    url: window.location.origin +'/api/user',
                    method: "GET",
                    params: {access_token:access_token}
                });
            },

            show : function(id) {
                console.log(access_token);
                return $http({
                    url: window.location.origin + '/api/user/' + id,
                    method: "GET",
                    params: {access_token:access_token}

                });
            },

            save : function(user_data) {
                console.log("UserSv save");

                return $http({
                    method: 'POST',
                    url: window.location.origin + '/api/user',
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    params: {access_token:access_token},
                    data: $.param(user_data)
                });
            },

            update: function(user_data){
                console.log("UserSv update function");
                user_data.access_token = access_token;
                console.log(user_data);
                return $http({
                    method: 'PUT',
                    url: window.location.origin + '/api/user/' + user_data.id,
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    data: $.param(user_data)
                });
            },
            destroy : function(id) {
                console.log("UserSv delete function")
                return $http({
                    url: window.location.origin + '/api/user/' + id,
                    method: "DELETE",
                    params: {access_token:access_token}
                });
            }
        }

    });