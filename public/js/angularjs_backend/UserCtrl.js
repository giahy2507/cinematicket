var adminApp = angular.module('adminApp');

adminApp.controller('UserCtrl', function($scope, $rootScope, $http, UserSv) {
    if (!localStorage.admin){
        var url = "http://" + window.location.host + "/admin";
        console.log(url);
        window.location.href = url;
    }

    $scope.user = {};
    $scope.users = [];
    $scope.admin = localStorage.admin;
    $scope.isAdd = false;
    
    UserSv.get()
        .success(function (data, status) {
            if (status == 203) {
                window.alert("Not authenticate");
            } else if (status == 200) {
                $scope.users = data;
            }
        })
        .error(function (data) {
            console.log("404 not found - blank access_token");
        });
    
    $scope.showModal = false;
    $scope.editModal = function(data){
        $scope.showModal = !$scope.showModal;
        $scope.user = data;
        $scope.btn_name = "Save";
        $scope.isAdd = false;
    };

    $scope.addNewModal = function(){
        $scope.showModal = !$scope.showModal;
        $scope.user = {};
        $scope.btn_name = "Add";
        $scope.isAdd = true;
    };

    $scope.delete = function(id){
        $scope.users = {};
        UserSv.destroy(id.toString())
            .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.users = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });
    };

    $scope.add = function(user){

        if (user.email == null){
            window.alert("Not enough information");
            return;
        }
        else{
            $scope.users = [];
            if ($scope.btn_name == "Add") {
                UserSv.save(user)
                    .success(function (data, status) {
                        if (status == 203) {
                            window.alert("Not authenticate");
                        } else if (status == 200) {
                            $scope.users = data;
                        }
                    })
                    .error(function (data) {
                        console.log("404 not found - blank access_token");
                    });
            }
            else {
                console.log(user) ;
                UserSv.update(user)
                    .success(function (data, status) {
                        if (status == 203) {
                            window.alert("Not authenticate");
                        } else if (status == 200) {
                            $scope.users = data;
                            console.log($scope.users) ;
                        }
                    })
                    .error(function (data) {
                        console.log("404 not found - blank access_token");
                    });
            } 
        }
        $scope.showModal = !$scope.showModal;
    };
});