var adminApp = angular.module('adminApp');

adminApp.controller('LoginCtrl', function($scope, $rootScope, $http, UserSv) {

    $scope.user_data = {};

    $scope.admin = localStorage.admin;
    
    $scope.loginRequest = function(){
        console.log("angular login request");
        console.log($scope.user_data);
        UserSv.login($scope.user_data)
            .success(function (data, status) {
                $scope.user_data = {};
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    localStorage.setItem('admin',data.user_name);
                    localStorage.setItem('admin_access_token',data.access_token);
                    window.location.replace(window.location.origin + "/admin/users");
                }
            })
            .error(function (data) {
                console.log(data);
            });
    };

    $scope.updateRequest = function(){
        console.log("angular save user request");
        UserSv.update($scope.user_data)
            .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.user_data = data;
                    window.alert("Your information is updated !")
                }
            })
            .error(function (data) {
                console.log(data);
            });
    };
});