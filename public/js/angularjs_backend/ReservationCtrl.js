console.log("loaded ReservationCtrl.js");

var CinematicketApp = angular.module('adminApp');

CinematicketApp.controller('ReservationCtrl', function($scope, ReservationSv, ShowtimeSv, SeatSv, UserSv) {
    if (!localStorage.admin){
        var url = "http://" + window.location.host + "/admin";
        console.log(url);
        window.location.href = url;
    }
    // object to hold all the reservation data
    $scope.reservations = [];
    $scope.query = "";

    ReservationSv.get()
        .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.reservations = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });

    $scope.set_query = function(query){
        $scope.query= query
    };

    $scope.showtimes = [];
    $scope.selectedShowtime = "";
    ShowtimeSv.get()
        .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.showtimes = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });

    $scope.seats = [];
    $scope.selectedSeat = "";
    SeatSv.get()
        .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.seats = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });

    $scope.users = [];
    $scope.selectedUser = "";
    UserSv.get()
        .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.users = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });

    $scope.changedShowtime =function(item){
        $scope.selectedShowtime = item;
    }
    $scope.changedSeat =function(item){
        $scope.selectedSeat = item;
    }
    $scope.changedUser =function(item){
        $scope.selectedUser = item;
    }
        

    $scope.delete = function(id){
        console.log("delete reservation");
        $scope.reservations = {};
        ReservationSv.destroy(id.toString())
            .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.reservations = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });
    };

    $scope.add = function(reservation){
        reservation.seat_id = $scope.selectedSeat.id;
        reservation.showtime_id = $scope.selectedShowtime.id;
        reservation.user_id = $scope.selectedUser.id;
        console.log(reservation);
        $scope.reservations = [];

        if ($scope.btn_name == "Add") {
            ReservationSv.store(reservation)
                .success(function (data, status) {
                    if (status == 203) {
                        window.alert("Not authenticate");
                    } else if (status == 200) {
                        $scope.reservations = data;
                    }
                })
                .error(function (data) {
                    console.log("404 not found - blank access_token");
                });
        }
        else {
            ReservationSv.update(reservation)
                .success(function (data, status) {
                    if (status == 203) {
                        window.alert("Not authenticate");
                    } else if (status == 200) {
                        $scope.reservations = data;
                    }
                })
                .error(function (data) {
                    console.log("404 not found - blank access_token");
                });
        }

        $scope.showModal = !$scope.showModal;
    };

    $scope.showModal = false;
    $scope.editModal = function(data){
        $scope.showModal = !$scope.showModal;
        $scope.reservation = data;
        $scope.btn_name = "Save";
        $scope.showtimes.forEach(function(entry) {
            if (entry.id == data.showtime_id){
                $scope.selectedShowtime = entry;
            }
        });
        $scope.seats.forEach(function(entry) {
            if (entry.id == data.seat_id){
                $scope.selectedSeat = entry;
            }
        });
        $scope.users.forEach(function(entry) {
            if (entry.id == data.user_id){
                $scope.selectedUser = entry;
            }
        });
          
    };

    $scope.addNewModal = function(){
        $scope.showModal = !$scope.showModal;
        $scope.reservation = {};
        $scope.btn_name = "Add";
        $scope.selectedShowtime = "";
        $scope.selectedSeat = "";
        $scope.selectedUser = "";
    };

});