console.log("loaded CinemaCtrl.js");

var CinematicketApp = angular.module('adminApp');

CinematicketApp.controller('CinemaCtrl', function($scope, CinemaSv) {
    if (!localStorage.admin){
        var url = "http://" + window.location.host + "/admin";
        console.log(url);
        window.location.href = url;
    }
    // object to hold all the cinema data
    $scope.cinemas = [];
    $scope.query = "";

    CinemaSv.get()
        .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.cinemas = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });

    $scope.set_query = function(query){
        $scope.query= query
    };

    $scope.delete = function(id){
        console.log("delete cinema");
        $scope.cinemas = {};
        CinemaSv.destroy(id.toString())
            .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.cinemas = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });
    };

    $scope.add = function(cinema){
        console.log("add cinema");
        console.log("cinema");
        $scope.cinemas = {};

        if ($scope.btn_name == "Add") {
            CinemaSv.store(cinema)
                .success(function (data, status) {
                    if (status == 203) {
                        window.alert("Not authenticate");
                    } else if (status == 200) {
                        $scope.cinemas = data;
                    }
                })
                .error(function (data) {
                    console.log("404 not found - blank access_token");
                });
        }
        else {
            CinemaSv.update(cinema)
                .success(function (data, status) {
                    if (status == 203) {
                        window.alert("Not authenticate");
                    } else if (status == 200) {
                        $scope.cinemas = data;
                    }
                })
                .error(function (data) {
                    console.log("404 not found - blank access_token");
                });
        }

        $scope.showModal = !$scope.showModal;
    };

    $scope.showModal = false;
    $scope.editModal = function(data){
        $scope.showModal = !$scope.showModal;
        $scope.cinema = data;
        $scope.btn_name = "Save";
    };

    $scope.addNewModal = function(){
        $scope.showModal = !$scope.showModal;
        $scope.cinema = {};
        $scope.btn_name = "Add";
    };

});