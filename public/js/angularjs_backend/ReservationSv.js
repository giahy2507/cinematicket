angular.module('ReservationSv', []).factory('ReservationSv', function($http) {
    var access_token = localStorage.admin_access_token;
    return {
        // get all Reservations
        get : function() {
            console.log("ReservationSv get function");
            return $http({
                url: window.location.origin +'/api/reservation',
                method: "GET",
                params: {access_token:access_token}
            });
        },
        show : function(id) {
            console.log("ReservationSv get function");
            return $http({
                url: window.location.origin +'/api/reservation/' + id,
                method: "GET",
                params: {access_token:access_token}
            });
        },
        destroy : function(id) {
            console.log("ReservationSv delete function")
            return $http({
                url: window.location.origin + '/api/reservation/' + id,
                method: "DELETE",
                params: {access_token:access_token}
            });
        },

        store : function(data) {
            console.log("ReservationSv store");
            console.log(data);
            return $http({
                    method: 'POST',
                    url: window.location.origin + '/api/reservation',
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    params: {access_token:access_token},
                    data: $.param(data)
                });

        },

        update : function(data) {
            console.log("ReservationSv update");
            return $http({
                    method: 'PUT',
                    url: window.location.origin + '/api/reservation/' + data.id,
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    params: {access_token:access_token},
                    data: $.param(data)
                });
        },
    };
});