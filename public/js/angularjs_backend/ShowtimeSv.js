angular.module('ShowtimeSv', []).factory('ShowtimeSv', function($http) {
    var access_token = localStorage.admin_access_token;
    return {
        // get all Showtimes
        get : function() {
            console.log("ShowtimeSv get function");
            return $http({
                url: window.location.origin +'/api/showtime',
                method: "GET",
                params: {}
            });
        },
        show : function(id) {
            console.log("ShowtimeSv get function");
            return $http({
                url: window.location.origin +'/api/showtime/' + id,
                method: "GET",
                params: {}
            });
        },
        destroy : function(id) {
            console.log("ShowtimeSv delete function")
            return $http({
                url: window.location.origin + '/api/showtime/' + id,
                method: "DELETE",
                params: {access_token:access_token}
            });
        },

        store : function(data) {
            console.log("ShowtimeSv store");
            console.log(data);
            return $http({
                    method: 'POST',
                    url: window.location.origin + '/api/showtime',
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    params: {access_token:access_token},
                    data: $.param(data)
                });

        },

        update : function(data) {
            console.log("ShowtimeSv update");
            return $http({
                    method: 'PUT',
                    url: window.location.origin + '/api/showtime/' + data.id,
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    params: {access_token:access_token},
                    data: $.param(data)
                });
        },
    };
});