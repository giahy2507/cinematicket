angular.module('StarSv', []).factory('StarSv', function($http) {
    var access_token = localStorage.admin_access_token;
    return {
        // get all Stars
        get : function() {
            console.log("StarSv get function");
            return $http({
                url: window.location.origin +'/api/star',
                method: "GET",
                params: {}
            });
        },
        show : function(id) {
            console.log("StarSv get function");
            return $http({
                url: window.location.origin +'/api/star/' + id,
                method: "GET",
                params: {}
            });
        },
        destroy : function(id) {
            console.log("StarSv delete function")
            return $http({
                url: window.location.origin + '/api/star/' + id,
                method: "DELETE",
                params: {access_token:access_token}
            });
        },

        store : function(data) {
            console.log("StarSv store");
            console.log(data);
            return $http({
                    method: 'POST',
                    url: window.location.origin + '/api/star',
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    params: {access_token:access_token},
                    data: $.param(data)
                });

        },

        update : function(data) {
            console.log("StarSv update");
            return $http({
                    method: 'PUT',
                    url: window.location.origin + '/api/star/' + data.id,
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    params: {access_token:access_token},
                    data: $.param(data)
                });
        },
    };
});