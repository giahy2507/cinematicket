console.log("loaded SeatCtrl.js");

var CinematicketApp = angular.module('adminApp');

CinematicketApp.controller('SeatCtrl', function($scope, SeatSv, CinemaSv) {
    if (!localStorage.admin){
        var url = "http://" + window.location.host + "/admin";
        console.log(url);
        window.location.href = url;
    }
    // object to hold all the seat data
    $scope.seats = [];
    $scope.query = "";

    SeatSv.get()
        .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.seats = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });

    $scope.cinemas = [];
    $scope.selectedCinema = "";

    CinemaSv.get()
        .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.cinemas = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });

    $scope.changedCinema =function(item){
        $scope.selectedCinema = item;
    }

    $scope.set_query = function(query){
        $scope.query= query
    };

    $scope.delete = function(id){
        console.log("delete seat");
        $scope.seats = {};
        SeatSv.destroy(id.toString())
            .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    $scope.seats = data;
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });
    };

    $scope.add = function(seat){
        $scope.seats = {};
        seat.cinema_id = $scope.selectedCinema.id;

        if ($scope.btn_name == "Add") {
            SeatSv.store(seat)
                .success(function (data, status) {
                    if (status == 203) {
                        window.alert("Not authenticate");
                    } else if (status == 200) {
                        $scope.seats = data;
                    }
                })
                .error(function (data) {
                    console.log("404 not found - blank access_token");
                });
        }
        else {
            SeatSv.update(seat)
                .success(function (data, status) {
                    if (status == 203) {
                        window.alert("Not authenticate");
                    } else if (status == 200) {
                        $scope.seats = data;
                    }
                })
                .error(function (data) {
                    console.log("404 not found - blank access_token");
                });
        }

        $scope.showModal = !$scope.showModal;
    };

    $scope.showModal = false;
    $scope.editModal = function(data){
        $scope.showModal = !$scope.showModal;
        $scope.seat = data;
        $scope.btn_name = "Save";
        $scope.cinemas.forEach(function(entry) {
            if (entry.id == data.cinema_id){
                $scope.selectedCinema = entry;
            }
        });
    };

    $scope.addNewModal = function(){
        $scope.showModal = !$scope.showModal;
        $scope.seat = {};
        $scope.btn_name = "Add";
        $scope.selectedCinema = "";
    };

});