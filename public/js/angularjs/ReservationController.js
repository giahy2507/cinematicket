/**
 * Created by HyNguyen on 1/3/16.
 */
/**
 * Created by HyNguyen on 1/2/16.
 */

console.log("loaded ReservationController.js");

var CinematicketApp = angular.module('CinematicketApp');

CinematicketApp.controller('ReservationController', function($scope, ReservationService) {

    // object to hold all the movie data
    $scope.reservations = [];

    $scope.get_more_detail = function(){
        console.log("get cinemas request");
        ReservationService.get_more_detail()
            .success(function (data, status) {
                $scope.reservations = [];
                if (status == 200) {
                    $scope.reservations = data;
                    console.log('reservation new: ', $scope.reservations );
                }
            })
            .error(function (data) {
                console.log('get movies error response ',data);
            });
    };
    $scope.get_more_detail();
});
