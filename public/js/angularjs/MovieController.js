/**
 * Created by HyNguyen on 1/1/16.
 */
console.log("loaded MovieController.js");

var CinematicketApp = angular.module('CinematicketApp');

CinematicketApp.controller('MovieController', function($scope, MovieService) {

    // object to hold all the movie data
    $scope.movies = [];
    $scope.movies_nowshowing = [];
    $scope.movies_comingsoon = [];
    $scope.cinemas_showtime_accord_movie_id = [];
    $scope.base_url = window.location.origin + "/";

    $scope.movie_detail = {};
    $scope.showtimes = [];
    $scope.available_seats = [];

    $scope.booking = {};

    $scope.query = "";

    $scope.set_query = function(query){
        $scope.query= query
    };

    $scope.showtime_change = function(){
        $scope.get_available_seats_accord_showtime_id_request($scope.booking.showtime_id);
        console.log($scope.booking);
    };
    $scope.seat_change = function(){
        console.log($scope.booking);
    };

    function convert_date(date_string){
        var monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];
        var date = new Date(date_string);
        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        console.log(day, monthNames[monthIndex], year);
        return monthNames[monthIndex] + " " + day.toString() + ", " + year.toString();
    };

    $scope.get_movies_request = function(){
        console.log("get movies request");
        MovieService.get()
            .success(function (data, status) {
                $scope.movies = [];
                if (status == 200) {
                    $scope.movies = data;
                    console.log('movies: ', $scope.movies );
                    for(i=0; i<data.length;i++){
                        var date_now = new Date();
                        var date_release = new Date(data[i].release_date);
                        if(date_release < date_now){
                            $scope.movies_nowshowing.push(data[i]);
                        }
                        else{
                            $scope.movies_comingsoon.push(data[i]);
                        }
                    }
                    console.log('movies movies_nowshowing: ', $scope.movies_nowshowing );
                    console.log('movies movies_commingsoon: ', $scope.movies_comingsoon );
                }
            })
            .error(function (data) {
                console.log('get movies error response ',data);
            });
    };
   // $scope.get_movies_request();

    $scope.get_movie_by_id = function (id){
        $scope.movie_detail = {};
        console.log("Get movie by id");
        MovieService.show(id)
            .success(function (data, status) {
                if (status == 200) {
                    if(data.length == 1){
                        $scope.movie_detail = data[0];
                        $scope.movie_detail.release_date = convert_date($scope.movie_detail.release_date);
                    }
                    console.log('movie: ',id.toString()," ", $scope.movie_detail );
                }
            })
            .error(function (data) {
                console.log('get movie by id error response ',data);
            });
    };

    $scope.get_cinemas_showtime_accord_movie_id_request = function (movie_id){
        $scope.cinemas_showtime_accord_movie_id = [];
        console.log("Get cinemas by movie_id request");
        MovieService.get_cinemas_showtime_acord_movie_id(movie_id)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.cinemas_showtime_accord_movie_id = data;
                    for(i=0;i<data.length;i++){
                        if(data[i].showtimes.length !=0){
                            console.log(' khac ko ');
                            $scope.showtimes = $scope.showtimes.concat(data[i].showtimes);
                        }
                    }
                    console.log('cinemas by id: ', $scope.cinemas_showtime_accord_movie_id);
                    console.log('show time by id: ', $scope.showtimes);
                }
            })
            .error(function (data) {
                console.log('get movie by id error response ',data);
            });
    };

    $scope.get_available_seats_accord_showtime_id_request = function (showtime_id){
        $scope.available_seats = [];
        console.log("Get cinemas by movie_id request");
        MovieService.get_available_seats_accord_showtime_id(showtime_id)
            .success(function (data, status) {
                if (status == 200) {
                    $scope.available_seats = data;
                    console.log('available seat ', $scope.available_seats);
                }
            })
            .error(function (data) {
                console.log('get movie by id error response ',data);
            });
    }

    $scope.book_ticket_requets = function(){
        //check user login
        if(localStorage.access_token == null | localStorage.access_token.localeCompare("")==0){
            window.alert("You need login before booking ticket ! ");
        }else{
            //request to reservation with access_token
            $scope.booking.access_token = localStorage.access_token;
            MovieService.booking($scope.booking)
                .success(function (data, status) {
                    if (status == 200) {
                        $scope.booking = {};
                        console.log('success booking ', data);
                        window.alert("Booking is Success !");
                        window.location.replace(window.location.origin+"/user");
                    }else if(status == 203){
                        window.alert("Not authentication");
                    }
                })
                .error(function (data) {
                    console.log('get movie by id error response ',data);
                });
        }
        //request to reservation
        console.log('request with ', $scope.booking);
    }
});
