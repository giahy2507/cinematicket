/**
 * Created by HyNguyen on 12/28/15.
 */

var CinematicketApp = angular.module('CinematicketApp');

CinematicketApp.controller('HeaderController', function($scope, $http, UserService) {

    $scope.user_name = localStorage.user_name;

    $scope.logoutRequest = function(){
        console.log("angular logout request");
        UserService.logout()
            .success(function (data, status) {
                $scope.user_data = {};
                if (status == 203) {
                } else if (status == 200) {
                    console.log("logout thanh cong");
                    localStorage.setItem("user_name","");
                }
                localStorage.setItem("access_token","");
                if(window.location.href.search("admin") != -1){
                    window.location.replace(window.location.origin + '/admin');
                }else {
                    window.location.replace(window.location.origin);
                }

            })
            .error(function (data) {
                console.log(data);
            });
    };

});