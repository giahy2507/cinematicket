/**
 * Created by HyNguyen on 1/2/16.
 */

console.log("this is CinemaService.js");

angular.module('CinemaService', []).factory('CinemaService', function($http) {
    var access_token = localStorage.access_token;
    return {
        // get all cinema
        get : function() {
            console.log("MovieService get function");
            return $http({
                url: window.location.origin +'/api/cinema',
                method: "GET",
                params: {}
            });
        },
        show : function(id) {
            console.log("MovieService get function");
            return $http({
                url: window.location.origin +'/api/cinema/' + id,
                method: "GET",
                params: {}
            });
        },
        get_by_showtime : function(showtime_id) {
            console.log("MovieService get function by showtime_id");
            return $http({
                url: window.location.origin +'/api/cinema_showtime_accord_id',
                method: "GET",
                params: {showtime_id:showtime_id}
            });
        },
    };
});