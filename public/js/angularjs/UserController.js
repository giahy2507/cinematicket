/**
 * Created by HyNguyen on 12/28/15.
 */
var CinematicketApp = angular.module('CinematicketApp');

CinematicketApp.controller('UserController', function($scope, $rootScope, $http, UserService) {

    // object to hold all the data for the new student form
    $scope.user_data = {};

    $scope.loginRequest = function(){
        console.log("angular login request");
        console.log($scope.user_data);
        UserService.login($scope.user_data)
            .success(function (data, status) {
                $scope.user_data = {};
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    localStorage.setItem('user_name',data.user_name);
                    localStorage.setItem('access_token',data.access_token);
                    window.location.replace(window.location.origin+"/user");
                }
            })
            .error(function (data) {
                console.log(data);
            });
    };

    $scope.registerRequest = function(){
        console.log("angular register request");
        UserService.register($scope.user_data)
            .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    // if successful, we'll need to refresh the comment list
                    $scope.loginRequest();
                }
            })
            .error(function (data) {
                console.log(data);
            });
    };

    $scope.updateRequest = function(){
        console.log("angular save user request");
        UserService.update($scope.user_data)
            .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    // if successful, we'll need to refresh the comment list
                    $scope.user_data = data;
                    window.alert("Your information is updated !")
                }
            })
            .error(function (data) {
                console.log(data);
            });
    };


    $scope.getuserRequest = function(user_id){
        console.log("angular get user request");
        $scope.user_data = {};
        UserService.show(user_id.toString())
            .success(function (data, status) {
                if (status == 203) {
                    window.alert("Not authenticate");
                } else if (status == 200) {
                    // if successful, we'll need to refresh the comment list
                    $scope.user_data = data;
                    localStorage.setItem('user_name',data.user_name);
                }
            })
            .error(function (data) {
                console.log("404 not found - blank access_token");
            });
    };


});