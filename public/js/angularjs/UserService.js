/**
 * Created by HyNguyen on 12/28/15.
 */

angular.module('UserService', []).factory('UserService', function($http) {
    var access_token = localStorage.access_token;
        return {
            login: function (user_data) {
                return $http({
                    method: 'POST',
                    url: window.location.origin + '/api/user/login',
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    data: $.param(user_data)
                });
            },

            logout : function() {
                return $http({
                    url: window.location.origin +'/api/user/logout',
                    method: "GET",
                    params: {access_token:access_token}
                });
            },

            register: function (user_data) {
                return $http({
                    method: 'POST',
                    url: window.location.origin + '/api/user/register',
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    data: $.param(user_data)
                });
            },

            // get all the students
            get : function() {
                return $http({
                    url: window.location.origin +'/api/user',
                    method: "GET",
                    params: {access_token:access_token}
                });
            },

            show : function(id) {
                console.log(access_token);
                return $http({
                    url: window.location.origin + '/api/user/' + id,
                    method: "GET",
                    params: {access_token:access_token}

                });
            },

            // save a student (pass in student data)
            save : function(studentData) {
                studentData['access_token'] = access_token;
                return $http({
                    method: 'POST',
                    url: window.location.origin + '/api/user',
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    data: $.param(studentData)
                });
            },

            update: function(studentData){
                studentData['access_token'] = access_token;
                return $http({
                    method: 'PUT',
                    url: window.location.origin + '/api/user' + '/' + studentData.id,
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    data: $.param(studentData)
                });
            },

            // destroy a student
            destroy : function(id) {
                $http({
                    url: window.location.origin + '/api/user/' + id,
                    method: "DELETE",
                    params: {access_token:access_token}
                });
            }
        }

    });