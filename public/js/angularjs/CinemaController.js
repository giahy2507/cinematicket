/**
 * Created by HyNguyen on 1/2/16.
 */

console.log("loaded CinemaController.js");

var CinematicketApp = angular.module('CinematicketApp');

CinematicketApp.controller('CinemaController', function($scope, CinemaService) {

    // object to hold all the movie data
    $scope.cinemas = [];

    $scope.cinemas_accord_movie_id = [];

    $scope.get_cinemas_request = function(){
        console.log("get cinemas request");
        CinemaService.get()
            .success(function (data, status) {
                $scope.cinemas = [];
                if (status == 200) {
                    $scope.cinemas = data;
                    console.log('cinema: ', $scope.cinemas );
                }
            })
            .error(function (data) {
                console.log('get movies error response ',data);
            });
    };
    $scope.get_cinemas_request();

});
