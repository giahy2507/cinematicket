/**
 * Created by HyNguyen on 1/2/16.
 */

console.log("this is ReservationService.js");

angular.module('ReservationService', []).factory('ReservationService', function($http) {
    var access_token = localStorage.access_token;
    return {
        // get all cinema
        get : function() {
            console.log("MovieService get function");
            return $http({
                url: window.location.origin +'/api/reservation',
                method: "GET",
                params: {access_token:access_token}
            });
        },
        show : function(id) {
            console.log("MovieService get function");
            return $http({
                url: window.location.origin +'/api/cinema/' + id,
                method: "GET",
                params: {}
            });
        },
        get_more_detail: function(){
            console.log("ReservationService get more function");
            return $http({
                url: window.location.origin +'/api/get_reservation_more_detail',
                method: "GET",
                params: {access_token:access_token}
            });
        }
    };
});