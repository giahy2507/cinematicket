/**
 * Created by HyNguyen on 1/1/16.
 */
console.log("this is MovieService.js");

angular.module('MovieService', []).factory('MovieService', function($http) {
    var access_token = localStorage.access_token;
    return {
        // get all movies
        get : function() {
            console.log("MovieService get function");
            return $http({
                url: window.location.origin +'/api/movie',
                method: "GET",
                params: {}
            });
        },
        show : function(id) {
            console.log("MovieService get function");
            return $http({
                url: window.location.origin +'/api/movie/' + id,
                method: "GET",
                params: {}
            });
        },

        booking : function(data){
            console.log("MovieService booking function");
            return $http({
                url: window.location.origin +'/api/reservation_userbooking',
                method: "POST",
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(data)
            });
        },

        get_cinemas_showtime_acord_movie_id : function(movie_id) {
            console.log("MovieService get function");
            return $http({
                url: window.location.origin +'/api/cinema_showtime_accord_movie_id',
                method: "GET",
                params: {movie_id:movie_id}
            });
        },
        get_available_seats_accord_showtime_id : function(showtime_id) {
            console.log("get_available_seats_accord_showtime_id function");
            return $http({
                url: window.location.origin +'/api/available_seat_accord_showtime_id',
                method: "GET",
                params: {showtime_id:showtime_id}
            });
        },
    };
});