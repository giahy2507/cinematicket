<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationModel extends Model
{
    //
    
    protected $table = 'reservation';

    protected $fillable = ['date', 'showtime_id', 'user_id', 'seat_id'];

    protected $hidden = ['created_at', 'updated_at', 'deleted'];

    public function showtime()
    {
        return $this->belongsTo('App\ShowtimeModel','showtime_id');
    }
    public function user()
    {
        return $this->belongsTo('App\UserModel','user_id');
    }
    public function seat()
    {
        return $this->belongsTo('App\SeatModel','seat_id');
    }
}
