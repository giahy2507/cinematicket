<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieModel extends Model
{
    //
    protected $table = 'movies';

    protected $fillable = ['name', 'release_date', 'genres', 'cover_url', 'thumbnail_url', 'poster_url'];

    protected $hidden = ['created_at', 'updated_at', 'deleted'];

    public function showtimes()
    {
    	return $this->hasMany('App\ShowtimeModel', 'movie_id');
    }

    public function stars()
    {
        return $this->belongsToMany('App\StarModel', 'star_movie_roles', 'movie_id', 'star_id');
    }
}
