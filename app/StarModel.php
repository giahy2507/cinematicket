<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StarModel extends Model
{
    //
    protected $table = 'stars';

    protected $fillable = ['name', 'birthday', 'gender', 'nationality', 'deathday'];
    
    protected $hidden = ['created_at', 'updated_at'];

    public function movies()
    {
        return $this->belongsToMany('App\MovieModel', 'App\Star_Movie_Role_Model', 'star_id', 'movie_id');
    }
}
