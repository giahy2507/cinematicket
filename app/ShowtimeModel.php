<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShowtimeModel extends Model
{
    //
    protected $table = 'showtimes';

    protected $fillable = ['type', 'time', 'movie_id', 'cinema_id'];

    protected $hidden = ['created_at', 'updated_at', 'deleted'];

    public function movie()
    {
        return $this->belongsTo('App\MovieModel','movie_id');
    }

    public function cinema()
    {
        return $this->belongsTo('App\CinemaModel','cinema_id');
    }
}
