<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckinModel extends Model
{
    protected $table = 'checkin';

    protected $fillable = ['id', 'reservation_id', 'created_at', 'updated_at'];

    protected $hidden = ['deleted'];

    public function reservation()
    {
        return $this->belongsTo('App\ReservationModel','reservation_id');
    }
}
