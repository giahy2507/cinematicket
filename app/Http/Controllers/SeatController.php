<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\SeatModel;
use App\ReservationModel;
use App\ShowtimeModel;

class SeatController extends Controller
{
    //
	public function index()
    {
        return response()->json(SeatModel::whereRaw('deleted = ?', [false])->get()->toArray(), 200);
    }

    public function available_seat_accord_showtime_id(Request $request)
    {
        $showtime_id = $request->showtime_id;
        $showtime =  ShowtimeModel::find($showtime_id);
        $cinema_id = $showtime->cinema_id;
        $seats = SeatModel::with(['reservations' => function($query) use ($showtime_id){
            $query->whereRaw('showtime_id = ? and deleted = ?',[$showtime_id,false]);
        }])->whereRaw('cinema_id= ? and deleted = ?', [$cinema_id,false])->get();

        $seats_available = [];
        for ($i=0;$i<count($seats);$i++){
            if(count($seats[$i]->reservations) == 0){
                array_push($seats_available,$seats[$i]);
            }
        }
        return response()->json($seats_available,200);
    }

    public function store(Request $request)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }
        else{
            if($user_array[0]['admin'] == false) {
                return response()->json(array(), 203);
            }else{
            	$seat = new SeatModel();
            	$seat->seat_number = $request->seat_number;
                $seat->cinema_id = $request->cinema_id;

                $seat->created_at = date("Y-m-d H:i:s");
                $seat->save();
                return $this->index();
            }
        }
    }

    public function show($id)
    {
        return response()->json(SeatModel::find($id),200);
    }



    public function update(Request $request, $id)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array('error' => 'Access denied.'),203);
        }else{
            if($user_array[0]['admin'] == false) {
                return response()->json(array('error' => 'Access denied.'), 203);
            }
            else {
                $seat = SeatModel::find($id);
                $seat->seat_number = $request->seat_number;
                $seat->cinema_id = $request->cinema_id;                

                $seat->updated_at = date("Y-m-d H:i:s");
                $seat->save();
                return $this->index();
            }
        }
    }

    public function destroy(Request $request, $id)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }else{
            if($user_array[0]['admin'] == false){
                return response()->json(array(),203);
            }else{
                //
                // SeatModel::destroy($id);

                $seat = SeatModel::find($id);
                $seat->deleted = true;
                $seat->save();
                return $this->index();
            }
        }
    }
}
