<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\ShowtimeModel;

class ShowtimeController extends Controller
{
    //
    public function index(Request $request)
    {
        return response()->json(ShowtimeModel::whereRaw('deleted = ?', [false])->get()->toArray(), 200);
    }

    public function store(Request $request)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }
        else{
            if($user_array[0]['admin'] == false) {
                return response()->json(array(), 203);
            }else{
            	$showtime = new ShowtimeModel();
            	$showtime->type = $request->type;
            	$showtime->time = $request->time;
            	$showtime->movie_id = $request->movie_id;
                $showtime->cinema_id = $request->cinema_id;

                $showtime->created_at = date("Y-m-d H:i:s");
                $showtime->save();
                return $this->index();
            }
        }
    }

    public function show(Request $request, $id)
    {
        return response()->json(ShowtimeModel::whereRaw('id = ? and deleted = ?', [$id, false])->get()->toArray(),200);
    }

    public function update(Request $request, $id)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array('error' => 'Access denied.'),203);
        }else{
            if($user_array[0]['admin'] == false) {
                return response()->json(array('error' => 'Access denied.'), 203);
            }
            else {
                $showtime = ShowtimeModel::find($id);
                $showtime->type = $request->type;
                $showtime->time = $request->time;
                $showtime->movie_id = $request->movie_id;
                $showtime->cinema_id = $request->cinema_id;
                
                $showtime->updated_at = date("Y-m-d H:i:s");
                $showtime->save();
                return $this->index();
            }
        }
    }

    public function destroy(Request $request, $id)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }else{
            if($user_array[0]['admin'] == false){
                return response()->json(array(),203);
            }else{
                // ShowtimeModel::destroy($id);

                $st = ShowtimeModel::find($id);
                $st->deleted = true;
                $st->save();
                return $this->index();
            }
        }
    }
}
