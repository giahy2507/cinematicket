<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\StarModel;

class StarController extends Controller
{
    //
	public function index(Request $request)
    {
        return response()->json(StarModel::whereRaw('deleted= ?', [false])->get()->toArray(), 200);
    }

    public function store(Request $request)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }
        else{
            if($user_array[0]['admin'] == false) {
                return response()->json(array(), 203);
            }else{
            	$star = new StarModel();
            	$star->name = $request->name;
            	$star->birthday = $request->birthday;
            	$star->gender = $request->gender;
                $star->nationality = $request->nationality;
                if ($request->has('deathday')) {
                    $star->deathday = $request->deathday;
                }


                $star->created_at = date("Y-m-d H:i:s");
                $star->save();
                return $this->index($request);
            }
        }
    }

    public function show(Request $request, $id)
    {
        return response()->json(StarModel::find($id),200);
    }

    public function update(Request $request, $id)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array('error' => 'Access denied.'),203);
        }else{
            if($user_array[0]['admin'] == false) {
                return response()->json(array('error' => 'Access denied.'), 203);
            }
            else {

                $star = StarModel::find($id);
                $star->name = $request->name;
                $star->birthday = $request->birthday;
                $star->gender = $request->gender;
                $star->nationality = $request->nationality;
                $star->deathday = $request->deathday;
                
                $star->updated_at = date("Y-m-d H:i:s");
                $star->save();
                return $this->index($request);
            }
        }
    }

    public function destroy(Request $request, $id)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }else{
            if($user_array[0]['admin'] == false){
                return response()->json(array(),203);
            }else{
                // StarModel::destroy($id);

                $star = StarModel::find($id);
                $star->deleted = true;
                $star->save();
                return $this->index($request);
            }
        }
    }
}
