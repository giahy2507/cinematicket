<?php

namespace App\Http\Controllers;

use App\CinemaModel;
use App\MovieModel;
use App\SeatModel;
use App\ShowtimeModel;
use App\User;
use App\ReservationModel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReservationController extends Controller
{
    //
    public function index(Request $request)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ? and deleted = ?',[$access_token, false])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }
        else{
            if($user_array[0]['admin'] == false) {
                $r = ReservationModel::whereRaw('user_id = ? and deleted = ?', [$user_array[0]['id'], false])->get()->toArray();
                return response()->json($r, 200);
            }else{
                return response()->json(ReservationModel::whereRaw('deleted = ?', [false])->get()->toArray(), 200);
            }
        }
    }

    public function get_reservation_more_detail(Request $request){
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ? and deleted = ?',[$access_token, false])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array('code'=>203),203);
        }
        else{
                $responser_data = [];
                $reservations = ReservationModel::whereRaw('user_id = ? and deleted = ?', [$user_array[0]['id'], false])->get()->toArray();
                foreach($reservations as $reservation){
                    $seat = SeatModel::find($reservation['seat_id']);
                    $showtime = ShowtimeModel::find($reservation['showtime_id']);
                    $moive = MovieModel::find($showtime->movie_id);
                    $cinema = CinemaModel::find($showtime->cinema_id);
                    $element = array('id' => $reservation['id'], 'seat_number' => $seat->seat_number, 'showtime'=> $showtime->time,'movie_id' => $moive->id , 'movie_name' => $moive->name, 'movie_url' => $moive->thumbnail_url, 'cover_url' => $moive->cover_url , 'poster_url' => $moive->poster_url , 'cinema_id'=>$cinema->id ,'cinema_name'=>$cinema->name, 'code'=>200);
                    array_push($responser_data, $element);
                }
               return response()->json($responser_data, 200);
        }
    }

    public function reservation_userbooking(Request $request){
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ? and deleted = ?',[$access_token, false])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }
        else{
            $reservation = new ReservationModel();
            $reservation->showtime_id = $request->showtime_id;
            $reservation->seat_id = $request->seat_id;
            $reservation->user_id = $user_array[0]['id'];
            $reservation->save();
            return response()->json(array($reservation),200);
        }
    }

    public function store(Request $request)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }
        else{
            $reservation = new ReservationModel();
            $reservation->date = date("Y-m-d");
            $reservation->showtime_id = $request->showtime_id;
            if ($request->has('user_id'))
                $reservation->user_id = $request->user_id;
            else
                $reservation->user_id = $user_array[0]['id'];

            $reservation->seat_id = $request->seat_id;
            $reservation->created_at = date("Y-m-d H:i:s");
            $reservation->save();
            return $this->index($request);
        }
    }

    public function show(Request $request, $id)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }else{
            if($user_array[0]['admin'] == false){
                $r = ReservationModel::whereRaw('user_id = ?, id = ?', [$user_array[0]['id'], $id])->get()->toArray();
                if (count($r)== 0)
                    return response()->json([], 203);
                return response()->json($r, 200);
            }else{
                return response()->json(ReservationModel::find($id),200);
            }
        }
    }

    public function update(Request $request, $id)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array('error' => 'Access denied.'),203);
        }else{
            if($user_array[0]['admin'] == false) {
                return response()->json(array('error' => 'Access denied.'), 203);
            }
            else{
                $reservation = ReservationModel::find($id);
                $reservation->date = date("Y-m-d");
                $reservation->showtime_id = $request->showtime_id;
                if ($request->has('user_id'))
                    $reservation->user_id = $request->user_id;
                else
                    $reservation->user_id = $user_array[0]['id'];
                $reservation->seat_id = $request->seat_id;
                $reservation->updated_at = date("Y-m-d H:i:s");
                $reservation->save();
                
                return $this->index( $request);
            }
        }
    }

    public function destroy(Request $request, $id)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }else{
            if($user_array[0]['admin'] == false){
                return response()->json(array(),203);
            }else{
                
                $reservation = ReservationModel::find($id);
                $reservation->deleted = true;
                $reservation->save();
                return $this->index( $request);
            }
        }
    }
}
