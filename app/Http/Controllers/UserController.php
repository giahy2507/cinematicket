<?php

namespace App\Http\Controllers;

use App\User;
//use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{


    public function test()
    {
        $data =  User::all()->toArray();
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }

    public function login(Request $request){
        $email = $request->email;
        $password = $request->password;

        $user_array = User::whereRaw('email = ? and password = ?',[$email, $password])->get()->toArray();
        if( count($user_array) == 0){
            return response()->json(array('access_token' => '', 'code' => 203),203);
        } else {
            $user = User::find($user_array[0]['id']);
            $user->access_token = str_random(50);
            $user->save();
            return response()->json(array('access_token' => $user->access_token, 'user_name'=> $user->user_name , 'code' => 200),200);
        }
    }

    public function adminLogin(Request $request){
        $email = $request->email;
        $password = $request->password;

        $user_array = User::whereRaw('email = ? and password = ?',[$email, $password])->get()->toArray();
        if( count($user_array) == 0){
            return response()->json(array('access_token' => ''),203);
        } else {
            if($user_array[0]['admin'] == false) {
                return response()->json(array('access_token' => ''),203);
            }else{
                $user = User::find($user_array[0]['id']);
                $user->access_token = str_random(50);
                $user->save();
                return response()->json(array('access_token' => $user->access_token, 'user_name'=> $user->user_name),200);
            }
        }
    }

    public function logout(Request $request){
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if( count($user_array) == 0){
            return response()->json(array(),203) ;
        } else {
            $user = User::find($user_array[0]['id']);
            $user->access_token = null;
            $user->save();
            return response()->json(array(),200) ;
        }
    }

    public function adminLogout(Request $request){
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if( count($user_array) == 0){
            return response()->json(array(),203) ;
        } else {
            $user = User::find($user_array[0]['id']);
            $user->access_token = null;
            $user->save();
            return response()->json(array(),200) ;
        }
    }

    public function register(Request $request){
        $email = $request->email;
        $user_array = User::whereRaw('email = ?',[$email])->get()->toArray();
        if(count($user_array) != 0){
            return response()->json(array(),406);
        }
        else{
            $user = new User();
            $user->user_name = $request->first_name." ".$request->last_name;
            $user->password = $request->password;
            $user->name = $user->user_name;
            $user->email = $request->email;
            $user->phone_number = $request->phone_number;
            $user->membership = "blue";
            $user->save();
            return response()->json($user,200);
        }
    }

    public function index(Request $request)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }
        else{
            if($user_array[0]['admin'] == false) {
                return response()->json($user_array[0], 200);
            }else{
                return response()->json(User::whereRaw('deleted = ?', [false])->get()->toArray(), 200);
            }
        }
    }

    public function store(Request $request)
    {
        $email = $request->email;
        $user_array = User::whereRaw('email = ?',[$email])->get()->toArray();
        if(count($user_array) != 0){
            return response()->json(array(),406);
        }
        else{
            $user = new User();
            if ($request->has('user_name')){
                $user->user_name = $request->user_name;
                $user->name = $user->user_name;
            }
            else {
                $user->user_name = $request->first_name." ".$request->last_name;
                $user->name = $request->first_name." ".$request->last_name;
            }
            $user->password = $request->password;
            $user->name = $user->user_name;
            $user->email = $request->email;
            $user->phone_number = $request->phone_number;
            $user->membership = "blue";
            $user->save();
            return $this->index($request);
        }
    }

    public function show(Request $request, $id)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }else{
            if($user_array[0]['admin'] == false){
                return response()->json($user_array[0],200);
            }else{
                return response()->json(User::find($id),200);
            }
        }
    }

    public function update(Request $request, $id)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }else{
            $user = null;
            if($user_array[0]['admin'] == false){
                // can't change email --> disable email change in front end
                $user = User::find($user_array[0]['id']);
            }else{
                $user = User::find($id);
                $user->membership = $request->membership;
            }
            $user->user_name = $request->name;
            if ($request->has('password'))
            {
                $user->password = $request->password;
            }
            $user->phone_number = $request->phone_number;
            $user->save();
            return $this->index($request);
        }
    }
    public function destroy(Request $request, $id)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }else{
            if($user_array[0]['admin'] == false){
                return response()->json(array(),203);
            }else{
                // User::destroy($id);
                $user = User::find($id);
                $user->deleted = true;
                $user->save();
                return $this->index($request);
            }
        }
    }

}
