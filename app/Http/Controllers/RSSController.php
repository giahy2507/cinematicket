<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use Goutte\Client;

use Roumen\Feed\Facades\Feed;


class RSSController extends Controller
{
    public function index(Request $request){
        $contents = array();

        $client = new Client();

        $crawler2 = $client->request('GET', 'http://lottecinemavn.com/vi-vn/su-kien.aspx');
        $crawler2->filter("span.dn_evnettt > a")->each(function ($node) use(&$contents) {
            array_push($contents,array('description'=> "Lotte Cinema's new events", 'link'=> "http://lottecinemavn.com/".$node->attr('href'), 'title'=> $node->attr('title'), 'author' => "Lotte Cinema"));
        });

        $crawler1 = $client->request('GET', 'https://www.galaxycine.vn/khuyen-mai');
        $crawler1->filter("li.col-4 > a")->each(function ($node) use(&$contents) {
            array_push($contents,array('description'=> "Galaxy Cinema's new events", 'link'=> $node->attr('href'), 'title'=> $node->attr('title'), 'author' => "Galaxy Cinema"));
        });

        // create new feed
        $feed = Feed::make();

        // cache the feed for 60 minutes (second parameter is optional)
        $feed->setCache(20, 'laravelFeedKey');

        // check if there is cached feed and build new only if is not
        if (!$feed->isCached())
        {
            // creating rss feed with our most recent 20 posts

            // set your feed's title, description, link, pubdate and language
            $feed->title = 'Finalproject 1212166 - 121404 RSS';
            $feed->description = 'Finalproject 1212166 - 121404 RSS';
            $feed->logo = 'http://yoursite.tld/logo.jpg';
            $feed->link = "http://cinematicket.herokuapp.com/";
            $feed->setDateFormat('datetime'); // 'datetime', 'timestamp' or 'carbon'
            $feed->pubdate = "1/7/2016";
            $feed->lang = 'en';
            $feed->setShortening(true); // true or false
            $feed->setTextLimit(100); // maximum length of description text

            foreach ($contents as $content)
            {
                // set item's title, author, url, pubdate, description and content
                $feed->add($content['title'], $content['author'], $content['link'], "1/7/2016", $content['description'], "");
            }

        }

        // first param is the feed format
        // optional: second param is cache duration (value of 0 turns off caching)
        // optional: you can set custom cache key with 3rd param as string
        return $feed->render('atom');

        // to return your feed as a string set second param to -1
        // $xml = $feed->render('atom', -1);


    }
}
