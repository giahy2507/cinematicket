<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\CinemaModel;
use App\AuditoriumModel as audi;

class CinemaController extends Controller
{
    //
    public function index(Request $request)
    {
        return response()->json(CinemaModel::whereRaw('deleted = ?', [false])->get()->toArray(),200);
    }

    public function store(Request $request)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }
        else{
            if($user_array[0]['admin'] == false) {
                return response()->json(array(), 203);
            }else{
            	$cine = new CinemaModel();
            	$cine->name = $request->name;
            	$cine->address = $request->address;
            	$cine->phone_number = $request->phone_number;

                $cine->created_at = date("Y-m-d H:i:s");
                $cine->save();
                return $this->index($request);
            }
        }
    }

    public function show(Request $request, $id)
    {
        return response()->json(MovieModel::find($id),200);
    }

    public function cinema_showtime_accord_movie_id(Request $request)
    {
        $movie_id = $request->movie_id;
        $data = CinemaModel::with(['showtimes' => function($query) use($movie_id){
            $query->where('movie_id','=', $movie_id);
        }])->get();
        return response()->json($data,200);
    }

    public function cinema_showtime_accord_id(Request $request)
    {
        $showtime_id = $request->showtime_id;
        $data = CinemaModel::with(['showtimes' => function($query) use($showtime_id){
            $query->where('id', '=' , $showtime_id);
        }])->get();
        return response()->json($data,200);
    }

    public function update(Request $request, $id)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array('error' => 'Access denied.'),203);
        }else{
            if($user_array[0]['admin'] == false) {
                return response()->json(array('error' => 'Access denied.'), 203);
            }
            else{
                $cine = CinemaModel::find($id);
                $cine->name = $request->name;
                $cine->address = $request->address;
                $cine->phone_number = $request->phone_number;
                
                $cine->updated_at = date("Y-m-d H:i:s");
                $cine->save();
                return $this->index($request);
            }
        }
    }

    public function destroy(Request $request, $id)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }else{
            if($user_array[0]['admin'] == false){
                return response()->json(array(),203);
            }else{
                
                // CinemaModel::destroy($id);
                $cine = CinemaModel::find($id);
                $cine->deleted = true;
                $cine->save();
                return $this->index($request);
            }
        }
    }
}
