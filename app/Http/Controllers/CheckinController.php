<?php

namespace App\Http\Controllers;

use App\CheckinModel;
use App\ReservationModel;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CheckinController extends Controller
{
    public function checkin_request(Request $request){

        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array('code'=> 203),203);
        }
        else{
            $reservations = ReservationModel::whereRaw('id = ? and deleted = ?',[$request->reservation_id, false])->get()->toArray();
            if(count($reservations) == 0){
                return response()->json(array('code'=> 203),203);
            }else{
                if($reservations[0]['user_id'] != $user_array[0]['id']){
                    return response()->json(array('code'=> 203),203);
                }else{
                    $reservation = ReservationModel::find($reservations[0]['id']);
                    $reservation->deleted = true;
                    $reservation->save();

                    $checkin = new CheckinModel();
                    $checkin->reservation_id = $reservation->id;
                    $checkin->save();
                    return response()->json(array('code'=> 200),200);
                }
            }
        }
    }
}
