<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Star_Movie_Role_Model as starrole;

class Star_Movie_Role_Controller extends Controller
{
    //
 	public function index(Request $request)
    {
        return response()->json(starrole::whereRaw('deleted = ?', [false])->get()->toArray(), 200);
    }

    public function store(Request $request)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }
        else{
            if($user_array[0]['admin'] == false) {
                return response()->json(array(), 203);
            }else{
            	$role = new starrole();
            	$role->role = $request->role;
            	$role->star_id = $request->star_id;
            	$role->movie_id = $request->movie_id;
            	
                $role->created_at = date("Y-m-d H:i:s");
                $role->save();
                return $this->index();
            }
        }
    }

    public function show(Request $request, $id)
    {
        return response()->json(starrole::find($id),200);
    }

    public function update(Request $request, $id)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array('error' => 'Access denied.'),203);
        }else{
            if($user_array[0]['admin'] == false) {
                return response()->json(array('error' => 'Access denied.'), 203);
            }
        }
        $role = starrole::find($id);
        $role->role = $request->role;
        $role->star_id = $request->star_id;
        $role->movie_id = $request->movie_id;
        $role->created_at = date("Y-m-d H:i:s");
        $role->save();
        return $this->index();
    }

    public function destroy(Request $request, $id)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }else{
            if($user_array[0]['admin'] == false){
                return response()->json(array(),203);
            }else{
                // starrole::destroy($id);

                $role = starrole::find($id);
                $role->deleted = true;
                $role->save();
                return $this->index();
            }
        }
    }   
}
