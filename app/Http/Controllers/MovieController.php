<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\MovieModel;


class MovieController extends Controller
{
    //
    public function index()
    {
        $data = MovieModel::with('stars')->where('deleted', 0)->get();
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }
        else{
            if($user_array[0]['admin'] == false) {
                return response()->json(array(), 203);
            }else{
            	$movie = new MovieModel();
            	$movie->name = $request->name;
            	$movie->release_date = $request->release_date;
            	$movie->genres = $request->genres;
            	if (!empty($request->cover_url)) {
            		$movie->cover_url = $request->cover_url;
            	}
                if (!empty($request->thumnail_url)) {
                    $movie->cover_url = $request->cover_url;
                }
                if (!empty($request->poster_url)) {
                    $movie->cover_url = $request->cover_url;
                }

                $movie->created_at = date("Y-m-d H:i:s");
                $movie->save();
                return $this->index();
            }
        }
    }

    public function show($id)
    {
        $data = MovieModel::with('stars')->where('id','=', $id)->get();
        return response()->json($data,200);
    }

    public function update(Request $request, $id)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array('error' => 'Access denied.'),203);
        }else{
            if($user_array[0]['admin'] == false) {
                return response()->json(array('error' => 'Access denied.'), 203);
            }
        }
        $movie = MovieModel::find($id);
        $movie->name = $request->name;
        $movie->release_date = $request->release_date;
        $movie->genres = $request->genres;
        if (!empty($request->cover_url)) {
            $movie->cover_url = $request->cover_url;
        }
        if (!empty($request->thumnail_url)) {
            $movie->cover_url = $request->cover_url;
        }
        if (!empty($request->poster_url)) {
            $movie->cover_url = $request->cover_url;
        }

        $movie->updated_at = date("Y-m-d H:i:s");
        $movie->save();
        return $this->index();
    }

    public function destroy(Request $request, $id)
    {
        $access_token = $request->access_token;
        $user_array = User::whereRaw('access_token = ?',[$access_token])->get()->toArray();
        if(count($user_array) == 0){
            return response()->json(array(),203);
        }else{
            if($user_array[0]['admin'] == false){
                return response()->json(array(),203);
            }else{
                // MovieModel::destroy($id);

                $movie = MovieModel::find($id);
                $movie->deleted = true;
                $movie->save();
                return $this->index();
            }
        }
    }
}
