<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/get_users', 'UserController@test');

//Route::get('/seatavai', 'SeatController@seat_available_accord_showtime_id');

Route::post('/api/user/login', 'UserController@login');
Route::get('/api/user/logout', 'UserController@logout');
Route::post('/api/user/register', 'UserController@register');

Route::post('/api/admin/login', 'UserController@adminLogin');
Route::get('/api/admin/logout', 'UserController@adminLogout');


Route::group(['prefix' => 'api', 'after'=>'allowOrigin'], function() {

    Route::resource('/user', 'UserController');
    Route::resource('/movie', 'MovieController');

    Route::get('/cinema_showtime_accord_movie_id', 'CinemaController@cinema_showtime_accord_movie_id');
    Route::get('/cinema_showtime_accord_id', 'CinemaController@cinema_showtime_accord_id');

    Route::resource('/cinema', 'CinemaController');
    Route::resource('/seat', 'SeatController');

    Route::get('/available_seat_accord_showtime_id', 'SeatController@available_seat_accord_showtime_id');


    Route::resource('/cinema', 'CinemaController');
    Route::resource('/seat', 'SeatController');
    Route::resource('/showtime', 'ShowtimeController');
    Route::resource('/star', 'StarController');
    Route::resource('/starrole', 'Star_Movie_Role_Controller');
    Route::resource('/reservation', 'ReservationController');
    Route::post('/reservation_userbooking', 'ReservationController@reservation_userbooking');

    Route::get('/get_reservation_more_detail', 'ReservationController@get_reservation_more_detail');

    Route::get('/checkin_request', 'CheckinController@checkin_request');
});

Route::group(['prefix' => 'admin', 'after'=>'allowOrigin'], function() {


	Route::get('/', function () {
        return view('backend.login');
    });
    Route::get('/showtimes', function () {
        return view('backend.showtimes');
    });
    Route::get('/users', function () {
        return view('backend.users');
    });
    Route::get('/cinemas', function () {
        return view('backend.cinemas');
    });
    Route::get('/reservations', function () {
        return view('backend.reservations');
    });
    Route::get('/movies', function () {
        return view('backend.movies');
    });
    Route::get('/stars', function () {
        return view('backend.stars');
    });
    Route::get('/seats', function () {
        return view('backend.seats');
    });
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {

    Route::get('auth/facebook', 'Auth\AuthController@redirectToProvider');

    Route::get('auth/facebook/callback', 'Auth\AuthController@handleProviderCallback');

    Route::get('/', function () {
        return view('frontend.index');
    });

    Route::get('/login', function () {
        return view('frontend.login');
    });

    Route::get('/logout', function(){
        return view('frontend.index');
    });

    Route::get('/register', function () {
        return view('frontend.register');
    });

    Route::get('/user',['as' => 'user', function () {

        if(Auth::user() != null){
            return view('frontend.user', array('access_token'=>Auth::user()->access_token));
        }
        else{
            return view('frontend.user', array('access_token'=> null));
        }
    }]);

    Route::get('/stars_movies','Star_Movie_Role_Controller@index');

    Route::get('/movies','MovieController@index');

    Route::get('/nowshowing', function(){
        return view('frontend.nowshowing');
    });
    Route::get('/comingsoon',function(){
        return view('frontend.comingsoon');
    });

    Route::get('/cinema',function(){
        return view('frontend.cinema');
    });

    Route::get('/search', function(\Illuminate\Http\Request $request){
        return view('frontend.search',array('query'=>$request->search_query));
    });

    Route::get('/movie/{movie_id}', function($movie_id){
        return view('frontend.detail', array('movie_id' => $movie_id));
    });

    Route::get('/rss', 'RSSController@index');

});

