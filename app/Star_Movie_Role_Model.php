<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Star_Movie_Role_Model extends Model
{
    //
    protected $table = 'star_movie_roles';

    protected $fillable = ['role', 'star_id', 'movie_id'];

    protected $hidden = ['created_at', 'updated_at', 'deleted'];

    public function star()
    {
        return $this->belongsTo('App\StarModel','star_id');
    }

    public function movie()
    {
        return $this->belongsTo('App\MovieModel','movie_id');
    }
}
