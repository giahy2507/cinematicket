<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeatModel extends Model
{
    //
    protected $table = 'seats';

    protected $fillable = ['seat_number', 'cinema_id'];
    
    protected $hidden = ['created_at', 'updated_at', 'deleted'];

    public function cinema()
    {
        return $this->belongsTo('App\CinemaModel','cinema_id');
    }

    public function reservations(){

        return $this->hasMany('App\ReservationModel', 'seat_id', 'id');
    }
}
