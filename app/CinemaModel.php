<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CinemaModel extends Model
{
    //

    protected $table = 'cinemas';

    protected $fillable = ['name', 'address', 'phone_number'];

    protected $hidden = ['created_at', 'updated_at', 'deleted'];

    public function showtimes(){

        return $this->hasMany('App\ShowtimeModel', 'cinema_id', 'id');
    }

    public function seats()
    {
    	return $this->hasMany('App\SeatModel', 'cinema_id', 'id');
    }
}
